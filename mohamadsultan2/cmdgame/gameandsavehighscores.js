let correct; //number to be guessed
let number; //number entered by user
let trials; //counts guesses
let dif; //stores difficulty string
let hs = []; //array of highscores
const fs = require('fs');

const readline = require('readline'); //readline module to read user input

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
 
const recursiveAsyncReadLine = function () { //function to access the starting menu, will be recalled unless 3 is entered
	trials = 0; //set counter at 0
	rl.question('Please Choose an option:\n'
        + '1) Start\n'
        + '2) View HS\n'
        + '3) Quit\n'
        , function (line) { //because of node.js asynchronous behavior the while loop does not work when a readline script is executed
            switch (line) { //to display the question in a loop and wait for the user’s input I executed the readline through  a recursion
                case '1': //switch case to check the user's answer
                    rl.question('Please Choose an option:\n' //if option is entered user has to choose a difficulty so a new question is asked
						+ '1) Easy \n'
						+ '2) Medium \n'
						+ '3) Hard\n', answer => {
					if (answer == 1) { //depending on the user's choice the range varies with the difficulty
					correct = Math.floor((Math.random() * 10) + 1);	
					console.log('the number is between 1 and 10');
					wronganswer();//initiate the function so the user can start guessing 
				} else if (answer == 2) {
					correct = Math.floor((Math.random() * 25) + 1);
					console.log('the number is between 1 and 25');
					wronganswer();
				} else if (answer == 3) {
					correct = Math.floor((Math.random() * 50) + 1);
					console.log('the number is between 1 and 50');
					wronganswer();
					}
					function wronganswer() { //asks the user to enter his guess and checks if true
					rl.question('Please Enter your guess', function (line2) { //readline through a recursion
						number = line2.substring(1); //remove the first character (<, >, =) to get the number only
						switch (line2.substring(0, 1)) { //check the first character to test
							case '<':
								if (correct < number) {
									console.log('yes');
									trials++; //add 1 to the counter
								} else {
									console.log('no');
									trials++;
								}
								wronganswer(); //call the function so the user can guess again
								break;
							
							case '>':
								if (correct > number) {
									console.log('yes');
									trials++;
								} else {
									console.log('no');
									trials++;
								}
								wronganswer();
								break;
								
							case '=':
								if (correct == number) {
									console.log('yes');
									trials++;
									hs.push(trials); // push score to the highscores array
									rl.question('Please enter your name : ', (answer2) => { //name is stored in answer2
										if (answer == 1) { //check first answer that sets difficulty
											dif = 'Easy';
										} else if (answer == 2) {
											dif = 'Medium';
										} else if (answer == 3) {
											dif = 'Hard';
										}
										trials = hs.splice(-1); //get last entered element from array (score) and store it in trials
										hs.push('Name: ' + answer2 + ' - Difficulty: ' + dif + ' - Score: ' + trials); // push highscore to array
										fs.appendFile('highscores.txt', 'Name: ' + answer2 + ' - Difficulty: ' + dif + ' - Score: ' + trials+',', (err) => {  
											if (err) throw err;
											});
										recursiveAsyncReadLine(); //call the main function so the user can choose what to do next
									});
								} else {
									console.log('no');
									trials++;
									wronganswer();
								}
								break;
							default:
								console.log('Please enter another'); //if user enters a wrong value he is asked to try again
								wronganswer();
						}
					});
					}
					});
                    break;
                case '2': //view highscores
					console.log('Highscores');
					fs.readFile('highscores.txt', 
								{ 'encoding': 'utf8' }, 
								function (err, data) {
							if (err) {
								console.log(err);
							} else {
								let contents = data;
								let arr = contents.split(',');
								Sort(arr, arr.length); //calls function Sort below to sort highscores
								for (let j in arr) {
								console.log(arr[j]); // print array after sorting
							}
								recursiveAsyncReadLine(); // call the main function so the user can choose what to do next
							}
					});
                    break;
                case '3':
                    return rl.close(); //if the user chooses option 3 to quit we close
                default:
                    console.log('No such option. Please enter another: '); //if user enters a different value he is asked to enter a valid one
					recursiveAsyncReadLine(); //call to the main function so the user can enter new value
            }
    });
};
 
recursiveAsyncReadLine(); // initiate the program


let Sort = function (arr, n) {	//sorting function 	
	if (n == 1) {
			return arr;
	}
        for (let i = 0; i < n - 1; i++) {
            if (arr[i].slice(-1) > arr[i + 1].slice(-1)) { //check the score ,which is the last part of the element, in order to sort
                let temp = arr[i];
                arr[i] = arr[i + 1];
                arr[i + 1] = temp;
            }
      }
        Sort(arr, n - 1);
};
