exports.data =  { "noor"  : { "Math" : [90,90,70],
						 "Phys" : [80,100,60],
						 "Chem" : [50, 100, 100]},
			 "Rabih"  : {"Math" : [77,88,70],
						 "Phys" : [80,100,60],
						 "Chem" : [50, 56, 100]},
			 "karim"  : {"Math" : [55,90,40],
						 "Phys" : [80,99,60],
						 "Chem" : [50, 100, 86]},
		}
		

var data1=this.data;

module.exports.getter = {
	
getter: function (name, course, index)
{
	for (var key in data1)
	{ 
		if(key===name)
		{
		var courses= data1[key];
			for (var key2 in courses)
			{
				if(key2===course)
				{
					var grade=courses[key2];
				    return grade[index];
				
				}
			}
		}
	}
}
};

module.exports.setter = {
setter: function (name, course, index, newgrade)
{
	for (var key in data1)
	{ 
		if(key===name)
		{
		var courses= data1[key];
			for (var key2 in courses)
			{
				if(key2===course)
				{
					var grade=courses[key2];
				    grade[index]=newgrade;
				
				}
			}
		}
	}
}

};