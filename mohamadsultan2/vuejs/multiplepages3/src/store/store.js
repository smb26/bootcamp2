import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        date: '',
        time: '',
        seat: ''
    },
    mutations: {
        change(state, date) {
          state.date = date
        },
        change2(state, time) {
            state.time = time
        },
        change3(state, seat) {
            state.seat = seat
        }
    },
    getters: {
        date: state => state.date,
        time: state => state.time,
        seat: state => state.seat
      }
})