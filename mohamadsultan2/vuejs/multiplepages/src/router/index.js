import Vue from 'vue'
import Router from 'vue-router'
import Page1 from '@/components/Page1'
import Page2 from '@/components/Page2'
import Page3 from '@/components/Page3'
import Page4 from '@/components/Page4'
import Page5 from '@/components/Page5'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'CreateDoc',
      component: Page1
    },
    {
      path: '/Page2',
      name: 'InsertDate',
      component: Page2
    },
    {
      path: '/Page3',
      name: 'InsertTime',
      component: Page3
    },
    {
      path: '/Page4',
      name: 'InsertSeat',
      component: Page4
    },
    {
      path: '/Page5',
      name: 'LastPage',
      component: Page5
    },
  ]
})
