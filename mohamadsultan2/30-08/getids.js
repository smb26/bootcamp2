var restify = require('restify');
var jsondata = require('./data.json');

var ids = [];

module.exports = function(req, res, next) {
	var exists = false;
	var index = null;

   for (var i = 0; i < jsondata['people'].length; i++) {
	ids.push(jsondata['people'][i]['id']);	
   }

	res.send(ids);
   
   next();	
};
