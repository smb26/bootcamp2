const nano = require('nano')('http://localhost:5984');
const db = nano.db.use('vuedatabase');

module.exports =  function(req,res,next) {
	db.view('vue','lastdoc',{descending: true, limit:1}).then((body) => {
        res.send(body);
        console.log(body['rows'][0]['value']);
	})
}