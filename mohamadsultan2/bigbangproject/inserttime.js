const nano = require('nano')('http://localhost:5984');
const db = nano.db.use('vuedatabase');

module.exports = function(req,res,next) {
  db.get(req.body.id).then(body =>{
    body["Time"] =req.body["time"];
    body["last modified"] = new Date();
    db.insert(body).then(data => {
      res.send(data);
    })
  })
}