var restify = require ('restify');
var server = restify.createServer();
var postdata = require('./savebooking');
var latestdata = require('./latestbooking');
var createdoc = require('./Create');
var lastdoc = require('./lastdoc');
var insert = require('./insert');
var inserttime = require('./inserttime');
var insertseat = require('./insertseat');
var insertall = require('./insertall');

server.use(restify.plugins.bodyParser({ mapParams: true }))
server.post('/postdata',postdata);
server.get('/latestdata',latestdata);
server.get('/lastdoc',lastdoc);
server.post('/createdoc',createdoc);
server.post('/insert',insert);
server.post('/inserttime',inserttime);
server.post('/insertseat',insertseat);
server.post('/insertall',insertall);
server.listen(8888, function(){
    console.log("running");
});