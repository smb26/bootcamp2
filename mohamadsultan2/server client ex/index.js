var jsondata =  require('./data.json')
var restify = require('restify');
var getfunction = require('./get')
var delfunction = require('./delete')
var insert = require('./create')
var update = require('./update')
var allids = require('./getids')
var alldata = require ('./getall')

var server= restify.createServer();
server.use(restify.plugins.queryParser({ mapParams: true}));
server.use(restify.plugins.bodyParser({ mapParams: true}));
server.get('/:id', getfunction);
server.del('/', delfunction);
server.post('/', insert);
server.put('/', update);
server.get('/', allids);
server.get('/all', alldata);

server.listen(8080, function() {
});