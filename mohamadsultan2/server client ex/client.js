const request = require('request');
const fs = require('fs');

var arr = [];

function getAll() {
	const options = {  
		url: 'http://localhost:8080/',
		method: 'GET',
		headers: {
			Accept: 'application/json',
			'Accept-Charset': 'utf-8',
			'User-Agent': 'my-reddit-client'
		}
	};

	request(options, function (err, res, body) {  
		let json = JSON.parse(body);
		getData(json[0], json.length);
	});
}
getAll();

function getData(data, length1) {
	const options = {  
		url: 'http://localhost:8080/' + data,
		method: 'GET',
		headers: {
			'Accept': 'application/json',
			'Accept-Charset': 'utf-8',
			'User-Agent': 'my-reddit-client'
		}
	};

	request(options, function (err, res, body) {  
		let json = JSON.parse(body);
		var id = json.id;
		var name = json.name;
		var age = json.age;
		var year = new Date().getFullYear() - age;
		
		insert(id, name, year, length1);
	});
}

function insert(id, name, year, length1) {
	var obj = {'id': id,'name': name,'year': year };
	arr.push(JSON.stringify(obj));
	fs.writeFile('updated.json', arr, (err) => {
		if (err) throw err;
		else delete1(id, length1);
		});
}

function delete1(i, length1) {
const headers = {
    'User-Agent': 'Super Agent/0.0.1',
    'Content-Type': 'application/x-www-form-urlencoded'
};

const options = {
    url: 'http://localhost:8080',
    method: 'DELETE',
    headers: headers,
    form: { 'id': i }
};

request(options, function (error, response, body) {
    if (!error && response.statusCode == 200) {
        console.log(body);
		if ( i + 1 <= length1)
		getData(i + 1, length1);
    }
});
}
