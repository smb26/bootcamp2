var restify = require('restify');
var jsondata =  require('./data.json');
const fs = require('fs');

module.exports = function(req, res, next) {
	
	var exists = false;
	var index = null;

   for (var i = 0; i < jsondata['people'].length; i++) {
		if (parseFloat(jsondata['people'][i]['id']) == req.body.id) {
			exists = true;
			index = i;
			break;
		}
	}

	if(exists ==false){
		var id = req.body.id;
		var name = req.body.name;
		var age = req.body.age;

		jsondata.people.push({        
			"id":id,
			"name":name,
			"age":age
			});
			
		txt = JSON.stringify(jsondata);
		fs.writeFile('./data.json', txt );

		res.send("added");
	} else if(exists == true) {
		res.send("id already exists");
		}
 
   next();
   
};