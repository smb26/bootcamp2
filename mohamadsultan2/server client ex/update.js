var restify = require('restify');
var jsondata =  require('./data.json');
const fs = require('fs');

module.exports = function(req, res, next) {
	
var exists = false;
var index = null;
		
   for (var i = 0; i < jsondata['people'].length; i++) {
		if (parseFloat(jsondata['people'][i]['id']) == req.body.id) {
			exists = true;
			index = i;
			break;
		}
	}

	if (exists == true){
		
		var id = req.body.id;
		var name = req.body.name;
		var age = req.body.age;

		jsondata.people[index].id=id;        
		jsondata.people[index].name=name;
		jsondata.people[index].age=age;
		
		txt = JSON.stringify(jsondata);
		fs.writeFile('./data.json', txt );

		res.send("updated");
		
	} else if (exists == false) {
		res.send('User not Found !');
		} 

   next();
   
};