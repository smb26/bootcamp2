var restify = require('restify');
var jsondata = require('./data.json');

module.exports = function(req, res, next) {
	var exists = false;
	var index = null;

   for (var i = 0; i < jsondata['people'].length; i++) {
		if (parseFloat(jsondata['people'][i]['id']) == req.params.id) {
			exists = true;
			index = i;
			break;
		}
   }

   if (exists == true) {
		res.send(jsondata['people'][index]);
   } else if (exists == false) {
		res.send('User not Found !');
   }
   next();	
};
