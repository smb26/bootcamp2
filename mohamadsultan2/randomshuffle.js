var array = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21];

module.exports = {
func1: function (arr)
{
	var ind1 = Math.floor(Math.random() * arr.length) + 0;  
	var ind2 = Math.floor(Math.random() * arr.length) + 0;  
	swap(arr,ind1,ind2);
}

};

function swap(arr,i,j)
{
	var temp =arr[i];
	arr[i]=arr[j];
	arr[j]=temp;
}