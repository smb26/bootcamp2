var shuffle = function (array) {
  var currentIndex = array.length; 
  var temporaryValue, randomIndex;

  while (currentIndex !== 0) {

    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

var arr = [2, 11, 37, 42];
arr = shuffle(arr);
console.log(arr);