const nano = require('nano')('http://localhost:5984');
const db = nano.db.use('apitest');

module.exports = function(req,res,next) {
	db.get(req.params.id).then((body) => {
		db.destroy(body["_id"],body["_rev"]).then((body) => {
			res.send(body);
		})
	})
}