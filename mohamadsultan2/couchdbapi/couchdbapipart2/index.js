var restify = require('restify')

var getall = require('./getallids')
var getuser = require('./getbyid')
var getbyname = require('./getbyname')
var delete1 = require('./delete')
var update = require('./update')

var server = restify.createServer()
server.use(restify.plugins.queryParser({ mapParams: true }))
server.use(restify.plugins.bodyParser({ mapParams: true }))
server.get('/', getall)
server.get('/:id', getuser)
server.get('/get/:name', getbyname)
server.del('/', delete1)
server.put('/:id', update)


server.listen(8080, function () {

})