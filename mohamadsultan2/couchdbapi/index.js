var restify = require('restify');
var getfunction = require('./get')
var getid = require('./getbyid')
var delfunction = require('./delete')
var update = require('./update')
var getname = require('./getbyname')

var server= restify.createServer();
server.use(restify.plugins.queryParser({ mapParams: true}));
server.use(restify.plugins.bodyParser({ mapParams: true}));
server.get('/', getfunction);
server.get('/:id', getid);
server.del('/', delfunction);
server.put('/', update);
server.get('/get/:name', getname);

server.listen(8080, function() {
});