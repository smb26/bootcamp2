/*global console*/ /* eslint no-console: "off" */ 
function reverseString(str) {
    var splitString = str.split("");
 
    var reverseArray = splitString.reverse(); 
 
    var joinArray = reverseArray.join(""); // var joinArray = ["o", "l", "l", "e", "h"].join("");
    
    console.log(joinArray);
}
 
reverseString("hello");