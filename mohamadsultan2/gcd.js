/*global console*/ /* eslint no-console: "off" */ 
var int1,int2;

const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question('Please enter the first integer : ', (answer1) => {
    rl.question('Please enter the second integer : ', (answer2) => {
			int1=parseInt(answer1,10);
			int2=parseInt(answer2,10);
			gcd(int1,int2);
        rl.close();
    });
});


var gcd = function (a, b) {
    if (a == 0)
        console.log(b);

    while (b != 0) {
        if (a > b)
            a = a - b;
        else
            b = b - a;
    }

    console.log(a);
}