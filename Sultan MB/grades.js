exports.data =  { "noor"  : { "Math" : [90,90,70],
						 "Phys" : [80,100,60],
						 "Chem" : [50, 100, 100]},
			 "Rabih"  : {"Math" : [77,88,70],
						 "Phys" : [80,100,60],
						 "Chem" : [50, 56, 100]},
			 "karim"  : {"Math" : [55,90,40],
						 "Phys" : [80,99,60],
						 "Chem" : [50, 100, 86]},
		}

var data1 = this.data;

exports.get = function (name,course,index){
	// looping over the names
	for(var key in data1) {
		if(key === name) {
			var courses = data1[key];
			for(var key2 in courses) {
				// if we find the course then we need to extract the grade
				// at the specified index
				if(key2 === course){
					var grade = courses[key2];
					return grade[index];
				}
			}
		}
	}

}

exports.set = function (name,course,index, value){
	// looping over the names
	for(var key in this.data) {
		// key mathces the name we enter the if statement
		if(key == name) {
			// storing the courses to access them in
			// the for loop
			var courses = this.data[key];
			// looping over the courses
			for(var key2 in courses) {
				if(key2 == course){
					var grade = courses[key2];
					key2[index] = value;
					console.log("The grade has been set to "+value);
				}
			}
		}
	}
	
}
