var arr = ['so','what','who','so','1', 1 ,'what','sultan','on','near','kill'];

// 2 for loops: the outer loop picks an element
// the inner loop will check if the elements in the array are identical
// if they are then we remove it
for (var i=0; i<arr.length; i++){
	for (var j=i+1; j<arr.length; j++){
		if (arr[i] === arr[j]){
			arr.splice(j,1);
		}
	}

}

console.log(arr);