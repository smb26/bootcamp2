var http = require('http')
var fs = require('fs')
var rawdata = fs.readFileSync('updated_data.json')
var myfile = JSON.parse(rawdata)
/* create a client class: get all users using the APIs & then remove the age
 attribute and add a "year" attribute that the value of the year the age represents
 for example if the age was 55 thent the year is 2018-55. Later put this info user
 to a new file called "updated_data".
 everytime a user gets a an insertion success in the new file, delete the user from the 
 original file using the APIs. If the user is not inserted, you don't delete the user and you 
  log an error
  */
// this function takes a callback function as a parameter
// what it does is the following: tries to do an http get request
// with specified options (like localhost, certain port....etc)
// if the request is successful it should return an array of IDs
// where it will be parsed and sent using the callback function
function getIDsModify(callback) {
	return http.get({
		host: 'localhost',
		port: '8080',
		path: '/api/all',
		headers: {
			'secret': 'bootcamp'
		}
	}, function (response) {
		// Continuously update stream with data
		var body = ''
		response.on('data', (data) => {
			body += data
			// for testing purposes
			// console.log('before parsing: '+body)
		})
		response.on('end', () => {
			// Data reception is done, callback with parsed JSON!
			var parsed = JSON.parse(body)
			// for testing purposes
			// console.log('After Parsing: '+parsed)
			callback(parsed)
		})
		response.on('error', () => {
			console.log(error)
		})
	})

}
//Delete people from old data.json
// http request to the server that will delete the person with the ID that 
// is specified as a paramter
function deletePeopleOld(id) {
	console.log('we are in delete: '+id)
	return http.request({
		host: 'localhost',
		port: '8080',
		path: '/api/people/'+id,
		method: 'delete',
		headers: {
			'secret': 'bootcamp'
		}
	}, (response) => {
	}).end()
	//console.log('we are out')
}

getIDsModify((data) => {
	// console logging for testing purposes
	// console.log(data)
	data.forEach(element => {
		return http.get({
			host: 'localhost',
			port: '8080',
			path: '/api/people/'+element,
			headers: {
				'secret': 'bootcamp'
			}
		}, function (response) {
			// Continuously update stream with array of IDs
			var arrID = ''
			response.on('data', (data) => {
				arrID += data
				console.log('before parsing person: '+arrID)
			})
			response.on('end', () => {
				// Data reception is done
				var parsed = JSON.parse(arrID)
				// checking if there exists an element in this array
				console.log('the id:'+parsed.id)
				if(parsed.id)
					write(parsed)
			})
			response.on('error', () => {
				console.log(error)
			})
		})
	})
})
// function that will handle writing data of the person, where person age field is replaced with
// year field, once the writing is done. a call to delete is processed
function write(person){
	// console.log for testing purposes
	// console.log('The person inside writeFunction: '+person)
	myfile.people.push({ id: person.id, name: person.name, year: new Date().getFullYear() - person.age })	
	var stringified = JSON.stringify(myfile, null, '\t')
	fs.writeFileSync('updated_data.json', stringified )
	deletePeopleOld(person.id)
}
