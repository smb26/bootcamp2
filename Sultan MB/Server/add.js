//requires
const fs = require('fs')
let rawdata = fs.readFileSync('data.json')
let data = JSON.parse(rawdata)

//check secret from json file
let checkSecret = (secret) => {
	return secret === data.secret ? false : true

}
//check secret and add according to result
exports.adding = (userId, userName, userAge, secret) => {
	if (checkSecret(secret))
		return 'Secret Incorrect'
	let user = { id: parseInt(userId), name: userName, age: parseInt(userAge) }
	// checking if we already have that ID, if we don't have it we push 
	if ((JSON.stringify(data.people).indexOf('"id":' + user.id)) < 0) {
		data.people.push(user)
	}
	// the 2 optional parameters are for formatting the JSON file
	var stringified = JSON.stringify(data, null, '\t')
	fs.writeFileSync('data.json', stringified)

}