var restify = require('restify')
//var Controller = require('./controller');
var get = require('./get')
var add = require('./add')
var deletion = require('./delete')
var update = require('./update')

// initiating the server
const server = restify.createServer({
	name: 'SultanStash',
	version: '1.0.0'
})
/*
The restify API Guide has this to say:
		You are responsible for calling next() in order to run the next handler in the chain.

The 'chain' they are referring to is the chain of handlers for each route. In general, each route 
will be processed by multiple handlers, in a specific order. The last handler in a chain does not 
really need to call next() -- but it is not safe to assume that a handler will always be the last
one. Failure to call all the handlers in a chain can result in either serious or subtle errors 
when processing requests.

Therefore, as good programming practice, your handlers should always call next() 
[with the appropriate arguments].
*/
server.use(restify.plugins.acceptParser(server.acceptable))
server.use(restify.plugins.queryParser())
server.use(restify.plugins.bodyParser())
//getting all people
server.get('/api/people', (req, res, next) => {
	res.send(200, get.getAll(req.header('secret')))
	return next()
})
// getting one by ID
server.get('/api/people/:id', (req, res, next) => {
	res.send(200, get.getByID(+req.params.id,req.header('secret')))
	return next()
})
// get all the IDs in JSON
server.get('/api/all', (req, res, next) => {
	res.send(200, get.getIDs(req.header('secret')))
	return next()
})
// adding a person to the people with the specified parameters
server.post('/api/people', (req, res, next) => {
	if (!req.body.id || !req.body.name || !req.body.age) {
		res.body = 'Error: Make sure you have the correct params(id, name, age)'
		return next()
	}
	res.send(201, add.adding(req.body.id, req.body.name, req.body.age, req.header('secret')))
	return next()
})
// deleting a person on the condition that an ID is provided
server.del('/api/people/:id', (req, res, next) => {
	if (!+req.params.id) {
		res.body = 'Error: Make sure you have the correct params(id)'
		return next()
	}
	res.send(204, deletion.remove(parseInt(+req.params.id), req.header('secret')))
	return next()
})
// updating parameters of a person with the condition that an ID is provided
server.put('/api/people', (req, res, next) => {
	if (!req.body.id) {
		send(400)
		res.body = 'Error: Make sure you have the correct params(id)'
		return next()
	}
	if (!req.body.name) {
		console.log('...1')
		res.send(200, update.updating(req.body.id, false, req.body.age, req.header('secret')))
		return next()
	}
	else if (!req.body.age) {
		console.log('...2')
		res.send(200, update.updating(req.body.id, req.body.name, false, req.header('secret')))
		return next()
	}
	else {
		console.log('...3')
		res.send(200, update.updating(req.body.id, req.body.name, req.body.age, req.header('secret')))
		return next()
	}
})
// the port the server will be on
server.listen(8080, () => {
	console.log('%s listening at %s', server.name, server.url)
})