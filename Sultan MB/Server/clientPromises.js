//this is just another similar client but implementing promises
var http = require('http')
var fs = require('fs')
var rawdata = fs.readFileSync('updated_dataPromises.json')
var myfile = JSON.parse(rawdata)

function getIDsModify() {
	try{
		return new Promise((resolve, reject) => {
			return http.get({
				host: 'localhost',
				port: '8080',
				path: '/api/all',
				headers: {
					'secret': 'bootcamp'
				}
			}, function (response) {
				// Continuously update stream with data
				var body = ''
				response.on('data', (data) => {
					body += data
					// for testing purposes
					// console.log('before parsing: '+body)
				})
				response.on('end', () => {
					// Data reception is done, callback with parsed JSON!
					var parsed = JSON.parse(body)
					// for testing purposes
					// console.log('After Parsing: '+parsed)
					resolve(parsed)
				})
				response.on('error', () => {
					console.log(error)
					reject()
				})
			})
		})
	}catch(error){
		console.log(error)
	}

}


function iteratingIDrequests(people){
	try{
		return new Promise( (resolve, reject) => {
			// console logging for testing purposes
			// console.log(data)
			people.forEach(element => {
				return http.get({
					host: 'localhost',
					port: '8080',
					path: '/api/people/'+element,
					headers: {
						'secret': 'bootcamp'
					}
				}, function (response) {
					// Continuously update stream with array of IDs
					var arrID = ''
					response.on('data', (data) => {
						arrID += data
						console.log('before parsing person: '+arrID)
					})
					response.on('end', () => {
						// Data reception is done
						var parsed = JSON.parse(arrID)
						// checking if there exists an element in this array
						console.log('the id:'+parsed.id)
						if(parsed.id)
							resolve(parsed)
						else
							reject()
					})
					response.on('error', () => {
						console.log(error)
					})
				})
			})
		})
	}catch(error){
		console.log(error)
	}
}


function write(person){
	// console.log for testing purposes
	// console.log('The person inside writeFunction: '+person)
	myfile.people.push({ id: person.id, name: person.name, year: new Date().getFullYear() - person.age })	
	var stringified = JSON.stringify(myfile, null, '\t')
	fs.writeFileSync('updated_dataPromises.json', stringified )
	deletePeopleOld(person.id)
}

//Delete people from old data.json
function deletePeopleOld(id) {
	console.log('we are in delete: '+id)
	return http.request({
		host: 'localhost',
		port: '8080',
		path: '/api/people/'+id,
		method: 'delete',
		headers: {
			'secret': 'bootcamp'
		}
	}, (response) => {
	}).end()
	//console.log('we are out')
}
// chain of promises
getIDsModify().then( function(data){
	iteratingIDrequests(data).then( function(person){
		console.log('calls')
		write(person)
	}) 
} )


/*
function iteratingIDs(people) {
    try {
        people.forEach(element => {
            return new Promise((resolve, reject) => {
                return http.get({
                    host: 'localhost',
                    port: '8080',
                    path: '/api/people/' + element,
                    headers: {
                        'secret': 'bootcamp'
                    }
*/