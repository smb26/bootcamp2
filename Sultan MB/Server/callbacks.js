// this is only a class that was created for me to visualize callbacks
var myCallback = function(data) {
	console.log('got data: '+data)
}

var usingItNow = function(callback) {
	callback('get it?')
}

usingItNow(myCallback)