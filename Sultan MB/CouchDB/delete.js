var errors = require('restify-errors')
var http = require('http')
const request = require('request')

//check secret and delete accordingly
exports.remove = (req, res, next) => {
	
	var id = req.params.id
	var boolean = true
	var options = {
		host: '127.0.0.1',
		port: '5984',
		path: '/data_center/'+id,
		method: 'GET',
		headers: {
			accept: 'application/json'
		}
	}
	http.get(options, function(resPon){
		let data = ''
		let parsed = ''
		resPon.on('data', function(chunk){
			data+= chunk

		})
		resPon.on('end', function(){
			parsed = JSON.parse(data)
			var revision = parsed._rev
			var options2 = {	url:'127.0.0.1:5984/data_center/'+id+'?rev='+revision+''}
			//console.log(options2)
			request.del('http://127.0.0.1:5984/data_center/'+id+'?rev='+revision, function(error, response, body){
				if(error){
					console.log("error: "+error)
				}else{
					console.log(response.body)
				}
			})

		})
	})
	if(boolean){
		return new errors.BadRequestError('ID doesn\'t exist!')
	}

}