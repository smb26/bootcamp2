const nano = require('nano')('http://localhost:5984')
const university = nano.db.use('university')
const config = require('./config.json')

function populate(){
	for(var i=0; i<30; i++){
		var name = config.name[ Math.floor(Math.random() * config.name.length)]
		var Lname = config.Lname[ Math.floor(Math.random() * config.Lname.length)]
		var nationality = config.nationality[ Math.floor(Math.random() * config.nationality.length)]
		var birth = config.birth[ Math.floor(Math.random() * config.birth.length)]

		university.insert({"type": "student","name": name,"Lname": Lname ,"nationality": nationality,"birth": birth}).then((body) => {
			console.log(body)
		})
	}

	for(var i=0; i<3; i++){
		var course = config.courses[i]
		var time1 = ''
		var time2 = ''
		for(var j=0; j<3; j++){
			var semester = config.semester[j]
			var credit1 = config.credit[ Math.floor(Math.random() * config.credit.length)]
			var credit2 = config.credit[ Math.floor(Math.random() * config.credit.length)]
			var check = true
			while(check){
				time1 = config.time[ Math.floor(Math.random() * config.time.length)]
				time2 = config.time[ Math.floor(Math.random() * config.time.length)]
				if (time1!==time2){
					check = false
				}
			}
		university.insert({"type": "course", "name": course, "credit":credit1, "semester": semester, "time": time1, "enrolled": []})			
		university.insert({"type": "course", "name": course, "credit":credit2, "semester": semester, "time": time2, "enrolled": []})
		}
	}
	
}

populate()