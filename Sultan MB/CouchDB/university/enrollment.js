const nano = require('nano')('http://localhost:5984')
const university = nano.db.use('university')
// var async = require('asyncawait/async');
// var await = require('asyncawait/await');
var async = require("async")

function enrolling(){
	university.view('course', 'semester_name_time').then( (body)=>{
		var tuples = []
		for(var i =0; i<body.rows.length; i=i+2){
			var temp = [body.rows[i],body.rows[i+1]]
			tuples.push(temp)
		}
		//console.log(tuples)
		//var index=0;
		async.forEachSeries(tuples, function(course, callback){

			university.view('student','ID').then( (body2)=>{
				async.forEachSeries(body2.rows, function(student, callback2){
					var mx = 3
					var mn = 0
					// rand will take values of 0,1,2
					var rand = Math.floor( Math.random() * (mx-mn)+mn )
					//console.log(rand)
					// if 0 then we will put the student in first section of the course
					if(rand==0){
						// we will get the course ny it is id, then we will take the return
						// which is called coursebody, we will push in the enrolled array the 
						// id of the student
						university.get(course[rand].id).then((coursebody)=>{
							// we are pushing the student id inside the coursebody
							coursebody.enrolled.push(student.id)
							university.insert(coursebody).then((whatever)=>{
								//nothing is done here
								callback2()
							})
						})
						//console.log('the course: '+course[rand].value+' the student: '+student.id)
					}// if 1 then we will put the student in second section of the course
					else if(rand==1){
						university.get(course[rand].id).then((coursebody)=>{
							// we are pushing the student id inside the coursebody
							coursebody.enrolled.push(student.id)
							university.insert(coursebody).then((whatever)=>{
								//nothing is done here
								callback2()
							})
						})
					}else if(rand==2){
						callback2()	
					}
				}, function(){
					//index++
					callback()				
				})
			})
		})
	})
}




enrolling()