const nano = require('nano')('http://localhost:5984')
const university = nano.db.use('university')
var async = require("async")

function grading(){
	university.view('course', 'enrolled').then( (body)=>{
		//console.log(body.rows[1].value)
		//var counter=0
		async.forEachSeries(body.rows, function(course, callback){
			console.log(course)
			async.forEachSeries(course.value, function(studentID, callback_2){
				//counter++
				var mx = 101
				var mn = 61
				// rand will take values of 0,1,2
				var randGrade = Math.floor( Math.random() * (mx-mn)+mn )
				university.insert({"type": "grade","course_ID": course.key[0],"student_ID": studentID ,"credit": course.key[1],"grade": randGrade}).then((body) => {
					//console.log(body)
				})
				//console.log(counter+'  '+studentID)
				callback_2()
			})
			callback()
		})
	})
}

grading()