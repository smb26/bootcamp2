var errors = require('restify-errors')
var http = require('http')
const nano = require('nano')('http://localhost:5984')
const db = nano.db.use('nano_test');

// http://127.0.0.1:5984/_utils/database.html?data_center/_design/details/_view/userNames

// return all people
exports.getAllIDs = (req, res, next) => {
	var options = {
		host: '127.0.0.1',
		port: '5984',
		path: '/data_center/_design/details/_view/userNames',
		method: 'GET',
		headers: {
			accept: 'application/json'
		}
	}
	let parsed = ''
	http.get(options, function(resPon){
		let data = ''
		resPon.on('data', function(chunk){
			data+= chunk

		})
		resPon.on('end', function(){
			parsed = JSON.parse(data)
			res.send(parsed)
		})
	})
}
// check secret then return a matching id if it exists
exports.getByID = (req, res, next) => {

	// this boolean is initialized as true, where if we find an ID that matches the request
	// we set it to false, so we don't excute the if statement that is after the loop
	var id = req.params.id
	var boolean = true
	var options = {
		host: '127.0.0.1',
		port: '5984',
		path: '/data_center/'+id,
		method: 'GET',
		headers: {
			accept: 'application/json'
		}
	}
	http.get(options, function(resPon){
		let data = ''
		let parsed = ''
		resPon.on('data', function(chunk){
			data+= chunk

		})
		resPon.on('end', function(){
			parsed = JSON.parse(data)
			res.send(parsed)
		})
	})
	if(boolean){
		return new errors.BadRequestError('ID doesn\'t exist!')
	}
}

exports.getByName = (req, res, next) =>{
	var name = req.params.name
	var options = {
		host: '127.0.0.1',
		port: '5984',
		path: '/data_center/_design/details/_view/IDbyName/?key="'+name+'"',
		method: 'GET',
		headers: {
			accept: 'application/json'
		}
	}
	let parsed = ''
	http.get(options, function(resP){
		let data = ''
		resP.on('data', function(chunk){
			data+= chunk

		})
		resP.on('end', function(){
			parsed = JSON.parse(data)
			res.send(parsed)
		})
	})

}

/* https://stackoverflow.com/questions/20089582/how-to-get-a-url-parameter-in-express
To get a URL parameter's value, use req.params

app.get('/p/:tagId', function(req, res) {
  res.send("tagId is set to " + req.params.tagId);
});
// GET /p/5
// tagId is set to 5


If you want to get a query parameter ?tagId=5, then use req.query

app.get('/p', function(req, res) {
  res.send("tagId is set to " + req.query.tagId);
});
// GET /p?tagId=5
// tagId is set to 5
*/