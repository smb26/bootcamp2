var restify = require('restify')
var get = require('./get')
// var add = require('./add')
// var deletion = require('./delete')

// initiating the server
const server = restify.createServer({
	name: 'SultanStash',
	version: '1.0.0'
})
server.use(restify.plugins.acceptParser(server.acceptable))
server.use(restify.plugins.queryParser())
server.use(restify.plugins.bodyParser())
// getting all people's IDs
server.get('/couchAPI/getAll', 	get.getAllIDs)
// getting one by ID
server.get('/couchAPI/get/:id', get.getByID)
// getting IDs with certain name
server.get('/couchAPI/byName/:name', get.getByName)
// deleting a person on the condition that an ID is provided with the revision ID
server.del('/couchAPI/delete/:id', deletion.remove)
// updating parameters of a person with the condition that an ID is provided,
// actually there is no update from the server side
// server.put('/couchAPI/update/:', update.updating)

// the port the server will be on
server.listen(8888, () => {
	console.log('incoming requests to', server.name, server.url)
})