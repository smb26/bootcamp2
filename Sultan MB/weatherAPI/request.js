var http = require('http')

var options = {
  host: 'api.openweathermap.org',
  path: '/data/2.5/weather',
  method: 'GET',
  headers: {
    'Content-Type': 'application/json'
  }
}
// the arguments array provided by user
var arg = process.argv.slice(2)
// arg[0] should be the name of the city
var city = arg[0]

// this exmple was retrieved from https://www.twilio.com/blog/2017/08/http-requests-in-node-js.html
http.get('http://api.openweathermap.org/data/2.5/forecast?q=' + city + '&APPID=6f3bdbdeb31b3c3512449dc013bc1d4f', (resp) => {
  // data that will be filled with the response from the server
  let data = ''
  resp.on('data', (info) => {
    // adding the info that we received to the variable data
    data += info
  })

  // The whole response has been received. Print out the result.
  resp.on('end', () => {
    // must parse the received data to be able to log it to the console
    // console.log(JSON.parse(data));
    var parsed = JSON.parse(data)
    // for (var key in parsed.list){
    // console.log(parsed.list[key].dt_txt+" temperature of "+parsed.city.name+" : "+parsed.list[key].main.temp);
    // }
    var i = 0;
    (function theLoop (i) {
      setTimeout(function () {
        console.log(parsed.list[i].dt_txt + ' temperature of ' + parsed.city.name + ' : ' + parsed.list[i].main.temp)
        if (++i && i < parsed.list.length - 1) { // If i > 0, keep going
          theLoop(i) // Call the loop again, and pass it the current value of i
        }
      }, 3000)
    })(0)//
    // printing the specific weather of
    // console.log("Today's "+JSON.parse(data).name+" temperature is: "+JSON.parse(data).main.temp);
  })
}).on('error', (err) => {
  console.log('Error: ' + err.message)
})
// helpful resources for this https://stackoverflow.com/questions/3583724/how-do-i-add-a-delay-in-a-javascript-loop
// http://shiya.io/send-http-requests-to-a-server-with-node-js/
// https://scottiestech.info/2014/07/01/javascript-fun-looping-with-a-delay/
