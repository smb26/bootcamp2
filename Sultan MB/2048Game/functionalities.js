var values = [2, 4];
// Exclusive work of SooSoo El-Shreer
// Copyrights reserved 09.28.2018
function running(){

	window.addEventListener("keydown", function(e){
		e.preventDefault();
		if(e.keyCode==37){
			// left arrow code
			var counter = 0;
			for(var i=1; i<17; i++){
				if(document.getElementById("box"+i).innerHTML != "" && (i%4)!=1){
					var destination = i-1;
					while(document.getElementById("box"+destination).innerHTML=="" && (destination%4)!=1 ){
						destination = destination-1;
					}
					if(document.getElementById("box"+destination).innerHTML == document.getElementById("box"+i).innerHTML){
						// storing the value of the number that will put inside the box
						var currentValue = parseInt(document.getElementById("box"+destination).innerHTML)*2;
						// storing the PREVIOUS value of the number that was in the box 
						// because we will use this number to change the value of a class-list
						var previousValue = parseInt(document.getElementById("box"+i).innerHTML);

						document.getElementById("box"+destination).innerHTML = currentValue;
						document.getElementById("box"+destination).classList.add("num"+currentValue);
						document.getElementById("box"+i).innerHTML = "";
						document.getElementById("box"+i).classList.remove("num"+previousValue);
						counter++;	
					}else if(document.getElementById("box"+destination).innerHTML== ""){
						var currentValue = parseInt(document.getElementById("box"+destination).innerHTML);
						var previousValue = parseInt(document.getElementById("box"+destination).innerHTML);

						document.getElementById("box"+destination).innerHTML = document.getElementById("box"+i).innerHTML;
						document.getElementById("box"+i).innerHTML = "";
						counter++;	
					}else if(document.getElementById("box"+(destination+1)).innerHTML== ""){
						document.getElementById("box"+(destination+1)).innerHTML = document.getElementById("box"+i).innerHTML;
						document.getElementById("box"+i).innerHTML = "";
						counter++;
					}
				}
			}

			if(counter>0){
				// deciding the location of the newly inserted value in the 4 X 4 grid
				var random =  Math.random(); 
				var location = Math.ceil(random * 16);
				while(document.getElementById("box"+location).innerHTML != ""){
					location = Math.ceil(Math.random() * 16);
				}7
				// deciding if the number inserted will be 2 or 4 based on probabilities
				var random2 =  Math.random(); 
				var value_2 = Math.ceil(random2 * 4);
				if (value_2<4){
					value_2 = 2;
				}else{
					value_2 = 4;
				}
				document.getElementById("box"+location).innerHTML = value_2;	
			}
		}else if(e.keyCode==38){
			// up arrow code
			var counter = 0;
			for(var i=5; i<17; i++){
				if(document.getElementById("box"+i).innerHTML != ""){
					var destination = i-4;
					while(document.getElementById("box"+destination).innerHTML=="" && (destination>4) ){
						destination = destination-4;
					}
					if(document.getElementById("box"+destination).innerHTML == document.getElementById("box"+i).innerHTML){
						document.getElementById("box"+destination).innerHTML = parseInt(document.getElementById("box"+destination).innerHTML)*2;
						document.getElementById("box"+i).innerHTML = "";
						counter++;
					}else if(document.getElementById("box"+destination).innerHTML== ""){
						document.getElementById("box"+destination).innerHTML = document.getElementById("box"+i).innerHTML;
						document.getElementById("box"+i).innerHTML = "";
						counter++;	
					}else if(document.getElementById("box"+(destination+4)).innerHTML== ""){
						document.getElementById("box"+(destination+4)).innerHTML = document.getElementById("box"+i).innerHTML;
						document.getElementById("box"+i).innerHTML = "";
						counter++;
					}
				}
			}
			
			if(counter>0){
				// deciding the location of the newly inserted value in the 4 X 4 grid
				var random =  Math.random(); 
				var location = Math.ceil(random * 16);
				while(document.getElementById("box"+location).innerHTML != ""){
					location = Math.ceil(Math.random() * 16);
				}7
				// deciding if the number inserted will be 2 or 4 based on probabilities
				var random2 =  Math.random(); 
				var value_2 = Math.ceil(random2 * 4);
				if (value_2<4){
					value_2 = 2;
				}else{
					value_2 = 4;
				}
				document.getElementById("box"+location).innerHTML = value_2;	
			}

		}else if(e.keyCode==39){
			// right arrow code
			var counter = 0;
			for(var i=16; i>0; i--){
				if(document.getElementById("box"+i).innerHTML != "" && (i%4)!=0){
					var destination = i+1;
					while(document.getElementById("box"+destination).innerHTML=="" && (destination%4)!=0 ){
						destination = destination+1;
					}
					if(document.getElementById("box"+destination).innerHTML == document.getElementById("box"+i).innerHTML){
						document.getElementById("box"+destination).innerHTML = parseInt(document.getElementById("box"+destination).innerHTML)*2;
						document.getElementById("box"+i).innerHTML = "";
						counter++;	
					}else if(document.getElementById("box"+destination).innerHTML== ""){
						document.getElementById("box"+destination).innerHTML = document.getElementById("box"+i).innerHTML;
						document.getElementById("box"+i).innerHTML = "";
						counter++;
					}else if(document.getElementById("box"+(destination-1)).innerHTML== ""){
						document.getElementById("box"+(destination-1)).innerHTML = document.getElementById("box"+i).innerHTML;
						document.getElementById("box"+i).innerHTML = "";
						counter++;
					}
				}
			}
			
			if(counter>0){
				// deciding the location of the newly inserted value in the 4 X 4 grid
				var random =  Math.random(); 
				var location = Math.ceil(random * 16);
				while(document.getElementById("box"+location).innerHTML != ""){
					location = Math.ceil(Math.random() * 16);
				}7
				// deciding if the number inserted will be 2 or 4 based on probabilities
				var random2 =  Math.random(); 
				var value_2 = Math.ceil(random2 * 4);
				if (value_2<4){
					value_2 = 2;
				}else{
					value_2 = 4;
				}
				document.getElementById("box"+location).innerHTML = value_2;	
			}

		}else if(e.keyCode==40){
			// down arrow code
			var counter = 0;
			for(var i=12; i>0; i--){
				if(document.getElementById("box"+i).innerHTML != ""){
					var destination = i+4;
					while(document.getElementById("box"+destination).innerHTML=="" && (destination<13) ){
						destination = destination+4;
					}
					if(document.getElementById("box"+destination).innerHTML == document.getElementById("box"+i).innerHTML){
						document.getElementById("box"+destination).innerHTML = parseInt(document.getElementById("box"+destination).innerHTML)*2;
						document.getElementById("box"+i).innerHTML = "";
						counter++;
					}else if(document.getElementById("box"+destination).innerHTML== ""){
						document.getElementById("box"+destination).innerHTML = document.getElementById("box"+i).innerHTML;
						document.getElementById("box"+i).innerHTML = "";
						counter++;
					}else if(document.getElementById("box"+(destination-4)).innerHTML== ""){
						document.getElementById("box"+(destination-4)).innerHTML = document.getElementById("box"+i).innerHTML;
						document.getElementById("box"+i).innerHTML = "";
						counter++;
					}
				}
			}

			if(counter>0){
				// deciding the location of the newly inserted value in the 4 X 4 grid
				var random =  Math.random(); 
				var location = Math.ceil(random * 16);
				while(document.getElementById("box"+location).innerHTML != ""){
					location = Math.ceil(Math.random() * 16);
				}7
				// deciding if the number inserted will be 2 or 4 based on probabilities
				var random2 =  Math.random(); 
				var value_2 = Math.ceil(random2 * 4);
				if (value_2<4){
					value_2 = 2;
				}else{
					value_2 = 4;
				}
				document.getElementById("box"+location).innerHTML = value_2;	
			}
		}

	})
}

function beginGame(){
	// need to randomly assign two blocks with values that are either 2 or 4
	// based on experience and trying the game, the value 2 has higher probability of 
	// showing up in the game it is definitely something like (or even higher) 3/4 for 2 to
	// while it is 1/4 for 4 

	var random =  Math.random(); 
	var value = Math.ceil(random * 4);
	var random2 =  Math.random(); 
	var value_2 = Math.ceil(random2 * 4);

	// now we need to randomly put these numbers in 2 tiles out of the 16 
	var x1 =  Math.ceil(Math.random() * 4);
	var y1 = Math.ceil(Math.random() * 4);
	var x2 =  Math.ceil(Math.random() * 4);
	var y2 = Math.ceil(Math.random() * 4);
	while(x1==x2 && y1==y2){
		x2 =  Math.ceil(Math.random() * 4);
		y2 = Math.ceil(Math.random() * 4);
	}
	if (value<4){
		value = 2;
	}else{
		value = 4;
	}
	if (value_2<4){
		value_2 = 2;
	}else{
		value_2 = 4;
	}
	printValues(value, x1, y1);
	printValues(value_2, x2, y2);


}
// this function takes a value which is the inner value of the tile
// x, y are the position of the tiles
function printValues(value, x, y){
	switch(x){
		// row 1
		case 1:
			switch(y){
				// column 1
				case 1:
					document.getElementById("box1").innerHTML = value;
					break;
				// column 2
				case 2:
					document.getElementById("box2").innerHTML = value;
					break;
				// column 3
				case 3:
					document.getElementById("box3").innerHTML = value;
					break;
				// column 4
				case 4:
					document.getElementById("box4").innerHTML = value;
					break;
			}
			break;
		// row 2
		case 2:
			switch(y){
				case 1:
					document.getElementById("box5").innerHTML = value;
					break;
				case 2:
					document.getElementById("box6").innerHTML = value;
					break;
				case 3:
					document.getElementById("box7").innerHTML = value;
					break;
				case 4:
					document.getElementById("box8").innerHTML = value;
					break;
			}
			break;
		// row 3
		case 3:
			switch(y){
				// column 1
				case 1:
					document.getElementById("box9").innerHTML = value;
					break;
				// column 2
				case 2:
					document.getElementById("box10").innerHTML = value;
					break;
				// column 3
				case 3:
					document.getElementById("box11").innerHTML = value;
					break;
				// column 4
				case 4:
					document.getElementById("box12").innerHTML = value;
					break;
			}
			break;
		// row 4
		case 4:
			switch(y){
				case 1:
					document.getElementById("box13").innerHTML = value;
					break;
				case 2:
					document.getElementById("box14").innerHTML = value;
					break;
				case 3:
					document.getElementById("box15").innerHTML = value;
					break;
				case 4:
					document.getElementById("box16").innerHTML = value;
					break;
			}
			break;		
	}
}