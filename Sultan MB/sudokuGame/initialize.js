
var g1 = [  [0,0,0,0,7,8,5,0,9], [0,1,0,3,0,5,4,0,6], [0,5,6,1,9,0,0,0,2], 
            [7,0,0,0,2,3,0,0,0], [8,0,0,0,0,0,0,0,2], [0,0,0,9,6,0,0,0,3],
            [3,0,0,0,6,4,9,5,0], [9,0,4,1,0,3,0,6,0], [6,0,7,5,8,0,0,0,0]
        ];
var solved = [  [4,3,2,7,1,9,8,5,6], [6,7,8,3,2,5,1,9,4], [5,1,9,4,8,6,3,7,2], 
                [7,9,6,8,3,1,2,4,5], [1,2,3,5,4,7,9,6,8], [8,4,5,6,9,2,7,1,3],
                [3,8,1,9,5,4,6,2,7], [2,6,4,1,7,3,5,8,9], [9,5,7,2,6,8,4,3,1]
            ];
function beginGame(){
    for(var i = 1; i<=9; i++){
        for(var j = 1; j<=9; j++){
            if(solved[i-1][j-1]==0){
                document.getElementById("box"+i+""+j).innerHTML = '';
            }else{
                document.getElementById("box"+i+""+j).innerHTML = solved[i-1][j-1];
            }
            
        }
    }
}

function running(element){
    window.addEventListener("keydown", function(e){
        e.preventDefault();
        if(e.keyCode==49){
            element.innerHTML = "1";
        }else if(e.keyCode==50){
            element.innerHTML = "2";
        }else if(e.keyCode==51){
            element.innerHTML = "3";
        }else if(e.keyCode==52){
            element.innerHTML = "4";
        }else if(e.keyCode==53){
            element.innerHTML = "5";
        }else if(e.keyCode==54){
            element.innerHTML = "6";
        }else if(e.keyCode==55){
            element.innerHTML = "7";
        }else if(e.keyCode==56){
            element.innerHTML = "8";
        }else if(e.keyCode==57){
            element.innerHTML = "9";
        }
        // EDIT: This will not work if you are working in strict mode ("use strict";)
        console.log(element);
        console.log(arguments.callee);
        this.removeEventListener('keydown', arguments.callee);
    });
}

function checking(){
    var done = true;
    for(var i = 1; i<=9; i++){
        for(var j = 1; j<=9; j++){
            // if a single cell is found to be empty then the game is not done
            if(document.getElementById("box"+i+""+j).innerHTML == ''){
                done = false;
            }
        }
    }

    if(done){
        // make a call to a function that will check if the solution is correct or not
        failOrSuccess();
    }
}

function failOrSuccess(){
    var success = true;
    // checking each row by itself
    for(var i=1; i<=9; i++){
        for(var j=1; j<=9; j++){
            if( document.getElementById("box"+i+""+j).innerHTML == document.getElementById("box"+i+""+1).innerHTML && j!=1){
                success = false;
            }
            if( document.getElementById("box"+i+""+j).innerHTML == document.getElementById("box"+i+""+2).innerHTML && j!=2){
                success = false;
            }
            if( document.getElementById("box"+i+""+j).innerHTML == document.getElementById("box"+i+""+3).innerHTML && j!=3){
                success = false;
            }
            if( document.getElementById("box"+i+""+j).innerHTML == document.getElementById("box"+i+""+4).innerHTML && j!=4){
                success = false;
            }
            if( document.getElementById("box"+i+""+j).innerHTML == document.getElementById("box"+i+""+5).innerHTML && j!=5){
                success = false;
            }
            if( document.getElementById("box"+i+""+j).innerHTML == document.getElementById("box"+i+""+6).innerHTML && j!=6){
                success = false;
            }
            if( document.getElementById("box"+i+""+j).innerHTML == document.getElementById("box"+i+""+7).innerHTML && j!=7){
                success = false;
            }
            if( document.getElementById("box"+i+""+j).innerHTML == document.getElementById("box"+i+""+8).innerHTML && j!=8){
                success = false;
            }
            if( document.getElementById("box"+i+""+j).innerHTML == document.getElementById("box"+i+""+9).innerHTML && j!=9){
                success = false;
            }
        }

    }
    if(!success){
        alert("incorrect solution in one of the rows.");
    }

    // checking each column by itself
    for(var i=1; i<=9; i++){
        for(var j=1; j<=9; j++){
            if( document.getElementById("box"+i+""+j).innerHTML == document.getElementById("box"+1+""+j).innerHTML && i!=1){
                success = false;
            }
            if( document.getElementById("box"+i+""+j).innerHTML == document.getElementById("box"+2+""+j).innerHTML && i!=2){
                success = false;
            }
            if( document.getElementById("box"+i+""+j).innerHTML == document.getElementById("box"+3+""+j).innerHTML && i!=3){
                success = false;
            }
            if( document.getElementById("box"+i+""+j).innerHTML == document.getElementById("box"+4+""+j).innerHTML && i!=4){
                success = false;
            }
            if( document.getElementById("box"+i+""+j).innerHTML == document.getElementById("box"+5+""+j).innerHTML && i!=5){
                success = false;
            }
            if( document.getElementById("box"+i+""+j).innerHTML == document.getElementById("box"+6+""+j).innerHTML && i!=6){
                success = false;
            }
            if( document.getElementById("box"+i+""+j).innerHTML == document.getElementById("box"+7+""+j).innerHTML && i!=7){
                success = false;
            }
            if( document.getElementById("box"+i+""+j).innerHTML == document.getElementById("box"+8+""+j).innerHTML && i!=8){
                success = false;
            }
            if( document.getElementById("box"+i+""+j).innerHTML == document.getElementById("box"+9+""+j).innerHTML && i!=9){
                success = false;
            }
        }
    }

    if(!success){
        alert("incorrect solution in one of the columns.");
    }

    /* There is another technique for checking validity of the 3 X 3 block of cells
        It goes by inserting the 9 cells of the block into an array.
        Then you sort this array.
        Once sorted you check that index 'i' of the array doesn't equal index 'i+1'.
        When done without having any equal values then you have a valid 3 X 3 block of cells */
    
    // the outer for loops are for traversing the blocks of 9 cells
    for(var i=1; i<=9; i+=3){
        for(var j=1; j<=9; j+=3){
            // these inner loops are for traversing the cells of the block
            for(var m = i; m < i+3; m++){
                for(var n = j; n < j+3; n++){
                    if( document.getElementById("box"+m+""+n).innerHTML == document.getElementById("box"+i+""+j).innerHTML && m!=i && n!=j){
                        success = false;
                    }
                    if( document.getElementById("box"+m+""+n).innerHTML == document.getElementById("box"+i+""+(j+1)).innerHTML && m!=i && n!=(j+1)){
                        success = false;
                    }
                    if( document.getElementById("box"+m+""+n).innerHTML == document.getElementById("box"+i+""+(j+2)).innerHTML && m!=i && n!=(j+2)){
                        success = false;
                    }
                    if( document.getElementById("box"+m+""+n).innerHTML == document.getElementById("box"+(i+1)+""+j).innerHTML && m!=(i+1) && n!=j){
                        success = false;
                    }
                    if( document.getElementById("box"+m+""+n).innerHTML == document.getElementById("box"+(i+1)+""+(j+1)).innerHTML && m!=(i+1) && n!=(j+1)){
                        success = false;
                    }
                    if( document.getElementById("box"+m+""+n).innerHTML == document.getElementById("box"+(i+1)+""+(j+2)).innerHTML && m!=(i+1) && n!=(j+2)){
                        success = false;
                    }
                    if( document.getElementById("box"+m+""+n).innerHTML == document.getElementById("box"+(i+2)+""+j).innerHTML && m!=(i+2) && n!=j){
                        success = false;
                    }
                    if( document.getElementById("box"+m+""+n).innerHTML == document.getElementById("box"+(i+2)+""+(j+1)).innerHTML && m!=(i+2) && n!=(j+1)){
                        success = false;
                    }
                    if( document.getElementById("box"+m+""+n).innerHTML == document.getElementById("box"+(i+2)+""+(j+2)).innerHTML && m!=(i+2) && n!=(j+2)){
                        success = false;
                    }
                    

                }
                if(!success){
                    alert("incorrect solution in one of the 3X3 blocks: [i,j]="+i+" "+j);
                }
            }

        }
    }

    if(!success){
        alert("incorrect solution");
    }else{
        alert("congratulations");
    }
    
}