import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
export default new Vuex.Store({

  state: {
    movieDoc: '',
    movieID: '',
    movieTitle: '',
    movieGenre: '',
    synopsis: '',
    releaseDate: '',
    director: '',
    moviePictureLink: '',
    sessionMoviesList: [],
    authenticated: true,
    jwtToken: '',
    user: ''
  },
  getters: {
    getmoviedoc (state) {
      return state.movieDoc
    },
    getmovieid (state) {
      return state.movieID
    },
    getmovietitle (state) {
      return state.movieTitle
    },
    getmoviegenre (state) {
      return state.movieGenre
    },
    getmoviesynopsis (state) {
      return state.synopsis
    },
    getmoviereleasedate (state) {
      return state.releaseDate
    },
    getmoviedirector (state) {
      return state.director
    },
    getmoviepicturelink (state) {
      return state.moviePictureLink
    },
    getmoviesessionslist (state) {
      return state.sessionMoviesList
    },
    getuserauthentication (state) {
      return state.authenticated
    },
    getjwttoken (state) {
      return state.jwtToken
    },
    getuser (state) {
      return state.user
    }
  },
  mutations: {
    setmovieDoc (state, data) {
      state.movieDoc = data
    },
    setmovieid (state, data) {
      state.movieID = data
    },
    setmovietitle (state, data) {
      state.movieTitle = data
    },
    setmoviegenre (state, data) {
      state.movieGenre = data
    },
    setsynopsis (state, data) {
      state.synopsis = data
    },
    setreleasedate (state, data) {
      state.releaseDate = data
    },
    setdirector (state, data) {
      state.director = data
    },
    setmoviepicturelink (state, data) {
      state.moviePictureLink = data
    },
    addsession (state, data) { // this will add a session to the sessionMoviesList array
      state.sessionMoviesList.push(data)
    },
    setauthentication (state, data) {
      state.authenticated = data
    },
    setjwttoken (state, data) {
      state.jwtToken = data
    },
    setuser (state, data) {
      state.user = data
    }
  }
})
