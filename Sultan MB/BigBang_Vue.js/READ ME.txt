To run this make sure to have the following installed:
Node.js (so you can use npm)
CouchDB (the project relies on this database to run)
Install Vue.js using npm
Install the dependencies stated in the package.json

From the 'Frontend' directory:
1- Open cmd
2- run the command 'npm install'
3- run the command 'npm run dev'

From the 'Backend' directory:
1- open cmd
2- run the command 'npm install'
3- run the command 'npm run dev'