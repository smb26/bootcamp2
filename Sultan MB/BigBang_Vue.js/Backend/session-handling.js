var errors = require('restify-errors');
var nano = require('nano')('http://localhost:5984');
var db = nano.db.use('bigbang_vue');
var jwt = require('jsonwebtoken');
exports.putSession = (req, res, next) => {
	var counter = 0
    /* To put a session in the database we must first determin if the user already has a session document in the database
		Otherwise we create a new document of type session and insert the session  */
	jwt.verify(req.token, 'secretAccessKey', (error, authenData) => {
		if (error) {
			res.send(403)
		} else {
			db.view('userSessions', 'getUserEmails', {descending:true}).then( function(body){
			  for (var i = 0; i < body.rows.length; i++) {
					if(body.rows[i].key == req.body['email']){
						counter++
						var doc = body.rows[i].value
						// we need to check if the movie was previously added to the user's session list
						if (doc.movies_ids.includes(req.body['movie'])) {
						  res.send()
						} else {
						  doc.movies_ids.push(req.body['movie'])
						  db.insert(doc).then((body) => {
							res.send()
						  })
						}
					}
				}
				// if we got out the for-loop and its inner if-statements then that means that we haven't found any existing
				// session for that user (the counter didn't get incremented). Therefore we create a dcoument of type session
				// and put the data inside it
				if(counter == 0){
					var doc = {"type":"session","email":req.body['email'],"movies_ids":[req.body['movie']],"creation":new Date()};
					console.log(doc);
					db.insert(doc).then((body) => {
						console.log(body);
						res.send();
					})	
				}
			})
			
			res.json({
				message: 'session added successfully..',
				authenData
			});
		}
	})
}

// db.view('userSessions', 'getUserEmails', {key: req.query.email}).then((body) =>{
// 	if(body.rows.length!=0){
// 		console.log(body.rows[0])
// 		res.send('success')
// 	} else {
// 		res.send('fail')
// 	}
// })

exports.getSessions = (req, res, next) => {
	var check = false
	db.view('userSessions', 'getUserEmails', {descending:true}).then((body) =>{
		for (var i = 0; i < body.rows.length; i++) {
			if(body.rows[i].key == req.query.email){
				check = true
				res.send(body.rows[i])
			}
		}
		if(!check){
			res.send('fail')
		}
	})
}

exports.deleteSession = (req, res, next) => {
	var counter = 0
    /* To put a session in the database we must first determin if the user already has a session document in the database
		Otherwise we create a new document of type session and insert the session  */
	jwt.verify(req.token, 'secretAccessKey', (error, authenData) => {
		if (error) {
			res.send(403)
		} else {
			db.view('userSessions', 'getUserEmails', {descending:true}).then( function(body){
			  for (var i = 0; i < body.rows.length; i++) {
					if(body.rows[i].key == req.body['email']){
						var doc = body.rows[i].value
						// we need to check if the movie was previously added to the user's session list
						if (doc.movies_ids.includes(req.body['movie'])) {
							// increment counter so we can later use it as a detection if we accessed this if-statement or not
							counter = counter+1
							console.log(counter)
							// find the position of the element we want to remove
							// then create a new array that has the updated array (which has that session deleted)
							console.log('array size'+doc.movies_ids.length+'')
							console.log('position of delete'+doc.movies_ids.indexOf(req.body['movie'])+'')
							var position = doc.movies_ids.indexOf(req.body['movie'])
							// extract that element from the array while maintaining the others
							doc.movies_ids.splice(position, 1)
							db.insert(doc).then((body2) => {
								// do nothing
							})
						}
					}
				}
				console.log(counter)
				// if counter stayed at 0 then we didn't do any deletion
				if (counter==0) {
					res.json({
						message: 'session couldn\'t be deleted..',
						authenData
					});
				} else {
					res.json({
						message: 'session deleted',
						authenData
					});
				}
			})
		}
	})
}