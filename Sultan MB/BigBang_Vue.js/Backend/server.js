var restify = require('restify')
var server = restify.createServer()
var booking_handling = require('./booking-handling')
var session_handling = require('./session-handling')
var login_handling = require('./login-handling')
var signup_handling = require('./signup-handling')
const corsMiddleware = require('restify-cors-middleware')

server.use(restify.plugins.bodyParser())
server.use(restify.plugins.queryParser())

const cors = corsMiddleware({
    origins: ['*'],
    allowHeaders: ['*']
})

server.pre(cors.preflight)
server.use(cors.actual)
server.post('/session', verifyToken, session_handling.putSession)
server.post('/removesession', verifyToken, session_handling.deleteSession)
server.post('/booking', booking_handling.putBooking)
server.post('/signup', signup_handling.signUser)
server.post('/login', login_handling.signUser)
server.get('/usersessions', session_handling.getSessions)
//server.get('/userbookings',)

// Format of Token 
// Authorization: Bearer <TOKEN>

// Verify Token 
function verifyToken(req, res, next) {
    // Get auth header value
    var bearerHeader = req.headers['authorization']
    if (typeof bearerHeader !== 'undefined') {
        // split at space character
        var bearer = bearerHeader.split(' ')
        // Get the token from the array
        var bearerToken = bearer[1]
        req.token = bearerToken
        next()
    } else {
        res.send(403,"You do not have rights to visit this page");
    }
}

server.listen(8888, function(){
    console.log('BigBang Server Listening @', server.name, server.url)
})