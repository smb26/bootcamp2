var restify = require('restify')
var get = require('./get')
var post = require('./post')
var docByID = require('./getDocByID')
var docField = require('./fieldInsertion')

const server = restify.createServer({
	name: 'BigBang',
	version: '1.0.0'
})

server.use( restify.plugins.acceptParser(server.acceptable) )
server.use( restify.plugins.queryParser() )
server.use( restify.plugins.bodyParser() )

server.get('/retrieve', get.getLast)
server.post('/post', post.putData)
// this was newly added on 10/04/2018
server.post('/getByID', docByID.getDoc)
server.post('/fieldAdd', docField.putField)

// the port the server will be on
server.listen(8888, () => {
	console.log('incoming requests to', server.name, server.url)
})