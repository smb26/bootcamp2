const nano = require('nano')('http://localhost:5984')
const db = nano.db.use('bigbang')
// this script is meant to receive a field from the front-end user & and ID of document
// the script will look for the matching document
// copy the values of the original document fields and values then add the value of the 
// single field that was provided by the front-end user
exports.putField = (req, res, next) => {

	db.get(req.body['id']).then( (body)=>{
		// Let me explain what am trying to achieve. Am getting the document by ID.
		// Once I have hold of it, then I can traverse the values inside it.
		// Since I created the document with empty fields at the beginning
		// it made it a bit trickier to handle, because instead of copying all 
		// the fields then add the newly provided field with its correspondng value
		// I need to copy the fields and monitor the field that must be updated
		// so I must put an if-condition on each field to see if it the one I will update.

		console.log(body);
		console.log("----------------------");
		console.log(body._id);
		console.log("----------------------")
		var field = req.body['field'];
		console.log(body[field]);
		// body[field]=req.body['value'];
		if ("weekday"==field){
			console.log("matched field");
			var doc_data = { "_id":body._id, "_rev":body._rev, "weekday":req.body['value'], "date":body.date, "time":body.time, "ticket_count":body.ticket_count, "ticket_price":body.ticket_price, "payment":body.payment, "voucher_Code":body.voucher_Code, "creation":body.creation, "last_modified":new Date()}
			db.insert(doc_data).then( (body2)=>{
				res.send(body2);
			} )
		}else if("date"==field){
			var doc_data = { "_id":body._id, "_rev":body._rev, "weekday":body.weekday, "date":req.body['value'], "time":body.time, "ticket_count":body.ticket_count, "ticket_price":body.ticket_price, "payment":body.payment, "voucher_Code":body.voucher_Code, "creation":body.creation, "last_modified":new Date()}
			db.insert(doc_data).then( (body2)=>{
				res.send(body2);
			} )
		}else if("time"==field){
			var doc_data = { "_id":body._id, "_rev":body._rev, "weekday":body.weekday, "date":body.date, "time":req.body['value'], "ticket_count":body.ticket_count, "ticket_price":body.ticket_price, "payment":body.payment, "voucher_Code":body.voucher_Code, "creation":body.creation, "last_modified":new Date()}
			db.insert(doc_data).then( (body2)=>{
				res.send(body2);
			} )
		}else if("ticket_count"==field){
			var doc_data = { "_id":body._id, "_rev":body._rev, "weekday":body.weekday, "date":body.date, "time":body.time, "ticket_count":req.body['value'], "ticket_price":parseInt(req.body['value'])*20, "payment":body.payment, "voucher_Code":body.voucher_Code, "creation":body.creation, "last_modified":new Date()}
			db.insert(doc_data).then( (body2)=>{
				res.send(body2);
			} )	
		}else if("payment"==field){
			var doc_data = { "_id":body._id, "_rev":body._rev, "weekday":body.weekday, "date":body.date, "time":body.time, "ticket_count":body.ticket_count, "ticket_price":body.ticket_price, "payment":req.body['value'], "voucher_Code":body.voucher_Code, "creation":body.creation, "last_modified":new Date()}
			db.insert(doc_data).then( (body2)=>{
				res.send(body2);
			} )	
		}else if("voucher_Code"==field){
			var doc_data = { "_id":body._id, "_rev":body._rev, "weekday":body.weekday, "date":body.date, "time":body.time, "ticket_count":body.ticket_count, "ticket_price":body.ticket_price, "payment":body.payment, "voucher_Code":req.body['value'], "creation":body.creation, "last_modified":new Date()}
			db.insert(doc_data).then( (body2)=>{
				res.send(body2);
			} )	
		}else{
			console.log("field provided is incorrect");
			res.send("Failure");
		}


		/* Another way of of checking the fields 
			var modified = false;
			for(i in body){
				if(body[i]==field){
					body[i] = req.body['value']
					modified = true;
				}
			}
			if(modified){
				body.last_modified = new Day();
			}
			db.insert(body).then( (body2)=>{
			
			} )
		*/
	})
}