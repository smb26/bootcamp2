function getLast(){
	var xreq = new XMLHttpRequest();
	var url = 'http://127.0.0.1:8888/retrieve';
	xreq.open('GET', url, true);
	var data = "";
	xreq.onreadystatechange = function() {//Call a function when the state changes.
		if(xreq.readyState == 4 && xreq.status == 200) {
			data = JSON.parse(xreq.responseText);
			var day = data['rows'][0]['doc']['weekday'].toLowerCase();
			var date = data['rows'][0]['doc']['date'];
			var time = data['rows'][0]['doc']['time'];
			var ticket_price = data['rows'][0]['doc']['ticket_price'];
			var ticket_count = data['rows'][0]['doc']['ticket_count'];
			var payment = data['rows'][0]['doc']['payment'];
			console.log(day+" "+date+" "+time+" "+ticket_price+" "+payment);
			selectDay(document.getElementById(day));
			var time_childs = document.getElementById("time-range").children;
			for(var i=0; i<document.getElementById("time-range").children.length; i++){
				if(time_childs[i].innerHTML.trim()==time.trim()){
					selectTime(time_childs[i]);
				}
			}
			var ticket_childs = document.getElementById("ticket-range").children;
			for(var i=0; i<document.getElementById("ticket-range").children.length; i++){
				if(ticket_childs[i].innerHTML.trim()==ticket_count.trim()){
					selectTicket(ticket_childs[i]);
				}
			}
			selectPayment(document.getElementById(payment));
	    }
	}
	xreq.send(null);
}