var data =  { "noor"  : { "Math" : [90,90,70],
						 "Phys" : [80,100,60],
						 "Chem" : [50, 100, 100]},
			 "Rabih"  : {"Math" : [77,88,70],
						 "Phys" : [80,100,60],
						 "Chem" : [50, 56, 100]},
			 "karim"  : {"Math" : [55,90,40],
						 "Phys" : [80,99,60],
						 "Chem" : [50, 100, 86]},
		}

var strObj;
try {
	strObj = JSON.stringify(data);
	console.log(strObj);
}catch(err){
	console.log(err);
}

console.log("---------------------||---------------------");

try {
	var parseObj = JSON.parse(strObj);
	console.log(parseObj);
}catch(err){
	console.log(err);
}