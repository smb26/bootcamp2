const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

var d = new Date();
var targetYear;
var month;
var day;
var currentYear;
// this will have the answer for the total days till the end of that year

var totalDays = 0;
rl.question('Which year you want me to find the days left for? ', (answer1) => {
	targetYear = parseInt(answer1);
	month = d.getMonth();
	// since we got the month we are currently at then we will simply look at the remaining months
	// for example if we are in october and we are provided with the year 2020
	// then we know that we should consider the remaining days of october, then November, 
	// December
	// then we calculate the remaining years till 2020
	day = d.getDate();
	currentYear = d.getFullYear();

	// I am checking if the year is a prior year or later year
	// (there is of course the condition that it is current year)
	if (targetYear > currentYear) {
		totalDays += daysInMonth(month,currentYear) - day;
		// the above line will give the remaining days of the current month

		for(var i = month + 1; i < 13; i++){
			totalDays += daysInMonth(i,currentYear);
		}
		for(var i = currentYear; i <= targetYear; i++){
			totalDays += daysOfYear(i);
		}
		console.log(totalDays);

	}

	rl.close();
});

// given a month and a year this will return the number of days that month of the year have
function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}

function daysOfYear(year) {
  return isLeapYear(year) ? 366 : 365;
}

function isLeapYear(year) {
     return year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0);
}

