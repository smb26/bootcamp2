const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question('Insert first number: ', (answer1) => {
    rl.question('Insert second number: ', (answer2) => {
        console.log("Answer is: " +  gcd(answer1, answer2));
        rl.close();
    });
});

function gcd(a, b) {
  return a == 0 ? b : gcd(b % a, a);
}
