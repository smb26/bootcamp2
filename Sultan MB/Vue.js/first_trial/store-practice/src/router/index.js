import Vue from 'vue'
import Router from 'vue-router'
import Form from '@/components/Form'
import Display from '@/components/Display'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
    },
    {
      path: '/Form',
      name: 'Form',
      component: Form
    },
    {
      path: '/Display',
      name: 'Display',
      component: Display
    }
  ]
})
