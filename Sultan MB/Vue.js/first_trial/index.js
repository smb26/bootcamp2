Vue.component('todo-item', {
    props: ['todo'],
    template: '<li>{{ todo.text }}</li>'
})

// emojify returns the corresponding emoji image
function emojify(name) {
	var out = `<img src="emojis/` + name + `.png">`
	return out
}

// cast returns a spell (function) that decorates the wizard
function cast(emoji) {
    if (!emoji) {
        emoji = "¯\\_(ツ)_/¯"
    }
	return function (wizard) {
		return emoji + wizard + emoji
	}
}

var app = new Vue({ 
	el: '#app',
	data: {
    		message: 'Hello Vue.js!',
    		message2: '',
    		todos: [
				{ text: 'Learn JavaScript' },
				{ text: 'Learn Vue' },
				{ text: 'Build something awesome' }
			],
			groceryList: [
				{ id: 0, text: 'Vegetables' },
				{ id: 1, text: 'Cheese' },
				{ id: 2, text: 'Whatever else humans are supposed to eat' }
			],
			wizard: emojify("wizard")
  		},
	
	methods: {
		reverseMessage: function () {
			this.message = this.message.split('').reverse().join('')
		},
		lumos: cast(emojify("lumos"))
	}
});


