const readline = require('readline');
// importing/requiring an outside file, so we can use a fucntion from it
const tempExport = require('./tempExport');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

//taking an argument from the command line in the form of 50C/99F/125K........etc
rl.question('Please input the temperature to convert ', (answer1) => {
	// making a call to the function that is imported from an outside file
	tempExport.temp(answer1);
	
	rl.close();
});