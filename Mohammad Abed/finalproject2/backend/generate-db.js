var nano = require('nano')('http://localhost:5984');
var docdesign = require('./docdesign');
var users = require('./users');

nano.db.create('bigbangwebsite').then((body) => {
    console.log('database bigbang created!');

    const bigbang = nano.use('bigbangwebsite');

    bigbang.insert(docdesign).then((body) => {
        console.log(body);
        bigbang.insert(users).then((body) => {
            console.log(body)
        });
    });
})