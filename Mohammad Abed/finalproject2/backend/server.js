var restify = require('restify');
const corsMiddleware = require('restify-cors-middleware')
var server = restify.createServer();
var userApi = require('./userApi')

server.use(restify.plugins.bodyParser());
server.use(restify.plugins.queryParser());

const cors = corsMiddleware({
    origins: ['*'],
    allowHeaders: ['*']
})

server.pre(cors.preflight)
server.use(cors.actual)

server.post('/login', userApi.checkuserexists, userApi.checkpasswordistrue);
server.post('/signup', userApi.checkuserexists, userApi.adduser);
server.post('/getuser', userApi.getuser);
server.post('/update', userApi.updateinfo);
server.post('/addtosession', userApi.addsession);
server.post('/getusers', userApi.getusers)
server.post('/updatesession', userApi.updatesession);
server.post('/getdocid', userApi.getdocid);
server.post('/getdoc', userApi.getdocument)
server.post('/deletemovie', userApi.deletemovie)
server.post('/addbooking', userApi.addbooking)

server.listen(8888, function () {
    console.log('%s listening at %s', server.name, server.url);
});