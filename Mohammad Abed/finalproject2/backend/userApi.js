var errors = require('restify-errors');
var nano = require('nano')('http://localhost:5984/bigbangwebsite');
//var userfound = false;

module.exports = {
    checkuserexists: function (req, res, next) {
        var username = req.body.name;
        nano.view('users', 'getusersbyname').then((body) => {
            if (body.rows.length > 0) {
                for (let i in body.rows) {
                    if (body.rows[i].key == username) {
                        req.pass = body.rows[i].value.password;
                        req.userfound = true;
                        console.log(req.body.name);
                        req.thebody = body;
                        return next();
                    }
                }

                req.userfound = false;
                return next();
            } else {
                req.userfound = false;
                return next();
            }
        });
    },

    checkpasswordistrue: function (req, res, next) {
        if (req.userfound == true) {
            if (req.body.password == req.pass) {
                res.send(req.thebody);
                return next();
            } else {

                return next(new errors.UnauthorizedError("Incorrect username or password."));
            }

        } else {
            return next(new errors.NotFoundError("User not found!"));
        }
    },
    adduser: function (req, res, next) {
        if (req.userfound == true) {
            return next(new errors.ConflictError("User already exists!"));
        } else if (req.userfound == false) {
            var newuser = {
                type: 'user',
                name: req.body.name,
                phonenumber: req.body.phonenumber,
                email: req.body.email,
                password: req.body.password
            }
            nano.insert(newuser).then((response) => {
                res.send(response);
                req.userfound = false;
                return next();
            })
        }
    },
    addsession: function (req, res, next) {
        var newuser = {
            type: 'session',
            userid: req.body.userid,
            movies: []
        }
        nano.insert(newuser).then((response) => {
            res.send(response);
        })

    },
    getuser: function (req, res, next) {
        nano.view('users', 'getusersbyname', {
            'key': req.body.name
        }).then((body) => {
            console.log(body);
            res.send(body);
        });
    },
    getdocid: function (req, res, next) {
        nano.view('users', 'getdocid', {
            'key': req.body.name
        }).then((body) => {
            console.log(body);
            res.send(body);
        });
    },
    updateinfo: function (req, res, next) {
        nano.get(req.body.id).then((body) => {
            nano.view('docdesign', 'getdocid').then((data) => {
                var ready = false;
                var exists = false;
                for (let i = 0; i < data.rows.length; i++) {
                    console.log(data.rows[i].key)
                    if (data.rows[i].key == req.body.name) {
                        exists = true;
                        break;
                    }
                }
                if (exists == false) {
                    if (req.body.name != "")
                        body["name"] = req.body.name;
                }
                if (req.body.phonenumber != "")
                    body["phonenumber"] = req.body.phonenumber;
                if (req.body.email != "")
                    body["email"] = req.body.email;
                ready = true;
                if (ready == true) {
                    nano.insert(body).then((data) => {
                        if (exists == true)
                            res.send('true');
                        else res.send('false')
                    })
                }
            });

        })
    },
    getusers: function (req, res, next) {
        nano.view('docdesign', 'getusers', {
            key: req.body.userid,
            include_docs: true
        }).then((body) => {
            res.send(body)
        });
    },
    updatesession: function (req, res, next) {
        nano.get(req.body.id).then((body) => {
            var found = false;
            for (let i = 0; i < body['movies'].length; i++) {
                if (body['movies'][i] == req.body.movies) {

                    found = true;
                    break;
                }
            }
            if (found == false) {
                body["movies"].push(req.body.movies);
                nano.insert(body).then((data) => {
                    res.send(data);
                })
            } else {
                return next(new errors.ConflictError("Movie Already in session"));
            }
        })
    },
    deletemovie: function (req, res, next) {
        nano.get(req.body.id).then((body) => {
            body["movies"].splice(req.body.movieid, 1);
            nano.insert(body).then((data) => {
                res.send(data);
            })

        })
    },
    getdocument: function (req, res, next) {
        nano.get(req.body.id).then((body) => {
            res.send(body);
        })
    },
    addbooking: function (req, res, next) {
        var newuser = {
            type: 'booking',
            userid: req.body.userid,
            title1: req.body.titleone,
            title2: req.body.titletwo,
            day: req.body.day,
            timeofmonth: req.body.date,
            time: req.body.time,
            numofseats: req.body.numofseats,
            paymentmethod: req.body.paymentmethod,
            vouchercode: req.body.vouchercode,
            total: req.body.total
        }
        nano.insert(newuser).then((response) => {
            res.send(response);
        })

    }

}