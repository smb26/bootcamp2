import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);
export default new Vuex.Store({

  state: {
    name: "",
    phone_num: "",
    email: "",
    userid: "",
    background: "red",
    account: false,
    nowshowing: false,
    session: false,
    index: 0,
    p1: false,
    p2: false,
    playlistinfo1: 0,
    playlistinfo2: 0,
    boolp1: false,
    boolp2: false,
    boolhome: false,
    getboolsession: false,
    sessionarray: [],
    moviedatasession: [],
    sessioncount: 0,
    username: [],
    loggedin: false,
    userid: ""


  },
  getters: {
    getboolp1(state) {
      return state.boolp1
    },
    getboolp2(state) {
      return state.boolp2
    },
    getboolhome(state) {
      return state.boolhome
    },
    getpli1(state) {
      return state.playlistinfo1
    },
    getpli2(state) {
      return state.playlistinfo2
    },
    getp1(state) {
      return state.p1;
    },
    getp2(state) {
      return state.p2;
    },
    getname(state) {
      return state.name;
    },
    getphonenum(state) {
      return state.phone_num;
    },
    getemail(state) {
      return state.email;
    },
    getuserid(state) {
      return state.userid;
    },
    getaccount(state) {
      return state.account;
    },
    getnowshowing(state) {
      return state.nowshowing
    },
    getsession(state) {
      return state.session;
    },
    getcurrentid(state) {
      return state.index;
    }
  },
  mutations: {
    setboolp1(state, payload) {
      state.boolp1 = payload
    },
    setboolp2(state, payload) {
      state.boolp2 = payload
    },
    setboolhome(state, payload) {
      state.boolhome = payload
    },
    setpli1(state, payload) {
      state.playlistinfo1 = payload
    },
    setpli2(state, payload) {
      state.playlistinfo2 = payload
    },
    setp1(state, payload) {
      state.p1 = payload
    },
    setp2(state, payload) {
      state.p2 = payload
    },
    setnsdata2(state, payload) {
      state.nowshowingdata2 = payload
    },
    setcurrentid(state, payload) {
      state.index = payload;
    },
    setname(state, payload) {
      state.name = payload;
    },
    setphonenum(state, payload) {
      state.phone_num = payload;
    },
    setemail(state, payload) {
      state.email = payload;
    },
    setuserid(state, payload) {
      state.userid = payload;
    },
    setaccount(state, payload) {
      state.account = payload;
    },
    setnowshowing(state, payload) {
      state.nowshowing = payload;
    },
    setsession(state, payload) {
      state.session = payload;
    }
  },


});
