import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import home from '@/components/home'
import nowshowing from '@/components/nowshowing'
import nowshowing2 from '@/components/nowshowing2'
import info from '@/components/info'
import session from '@/components/session'
import account from '@/components/account'
import login from '@/components/login'
import signup from '@/components/signup'
import booking from '@/components/booking'
import footernav from '@/components/footernav'
import refundreturn from '@/components/refundreturn'
import privacypolicy from '@/components/privacypolicy'
import contactus from '@/components/contactus'
import reciept from '@/components/reciept'

Vue.use(Router)

export default new Router({
  routes: [{
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/reciept',
      name: 'reciept',
      component: reciept
    },
    {
      path: '/home',
      name: 'home',
      component: home
    },
    {
      path: '/booking',
      name: 'booking',
      component: booking
    },
    {
      path: '/nowshowing',
      name: 'nowshowing',
      component: nowshowing
    },
    {
      path: '/nowshowing2',
      name: 'nowshowing2',
      component: nowshowing2
    },
    {
      path: '/info',
      name: 'info',
      component: info
    },
    {
      path: '/session',
      name: 'session',
      component: session
    },
    {
      path: '/account',
      name: 'account',
      component: account
    },
    {
      path: '/login',
      name: 'login',
      component: login
    },
    {
      path: '/signup',
      name: 'signup',
      component: signup
    },
    {
      path: '/footernav',
      name: 'footernav',
      component: footernav
    },

    {
      path: '/refund-return-policy',
      name: 'refundreturn',
      component: refundreturn
    },
    {
      path: '/privacy-policy',
      name: 'privacypolicy',
      component: privacypolicy
    },
    {
      path: '/contactus',
      name: 'contactus',
      component: contactus
    }



  ]
})
