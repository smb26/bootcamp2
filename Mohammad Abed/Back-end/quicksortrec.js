var arr= [1,1,2,3,1,2,3,4,3,2,1,2,3,4,5,6,7,8,1,2,3,2,1,2,4,11,12,13];

function swap(arr, i, j){
    var temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp;
}


function quickSort(arr, low, high) {

    if (arr.length > 1) {

        var pivot   = arr[Math.floor((high + low) / 2)],
        i       = low,
        j       = high;

    while (i <= j) {

        while (arr[i] < pivot) {
            i++;
        }

        while (arr[j] > pivot) {
            j--;
        }

        if (i <= j) {
            swap(arr, i, j);
            i++;
            j--;
        }
    }


        if (low < i - 1) {
            quickSort(arr, low, i - 1);
        }

        if (i < high) {
            quickSort(arr, i, high);
        }

    }

    return arr;
}


// first call
var result = quickSort(arr, 0, arr.length - 1);
console.log(result);