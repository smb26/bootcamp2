const nano = require('nano')('http://localhost:5984');
const db = nano.db.use('recipes');

module.exports = function(req,res,next) {
	db.get(req.params.id).then((body) => {
		body["name"] = req.body.name;
		body["age"] = req.body.age;
		db.insert(body).then((data)=>{
			res.send(data);
		})
	})
}