var restify = require('restify')
var createuser = require('./createuser')
var getuser = require('./getbyid')
var updateuser = require('./updateuser')
var deleteuser = require('./deleteuser')
var getall = require('./getallids')
var getbyname = require('./getbyname')


var server = restify.createServer()
server.use(restify.plugins.queryParser({ mapParams: true }))
server.use(restify.plugins.bodyParser({ mapParams: true }))
server.post('/db/user/create', createuser)
server.get('/db/user/get/:id', getuser)
server.put('/db/user/update/:id', updateuser)
server.del('/db/user/delete/:id', deleteuser)
server.get('/db/users/all', getall)
server.get('/db/user/byname/:name', getbyname)


server.listen(8080, function () {

})
