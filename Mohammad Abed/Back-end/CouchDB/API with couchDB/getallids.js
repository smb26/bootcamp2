var http = require('http')

module.exports = function (req, res, next) {
  var options = {
    host: '127.0.0.1',
    port: '5984',
    path: '/recipes/_design/namedoc/_view/allids',
    method: 'GET',
    headers: {
      'secret': 'bootcamp'
    }
  }

  http.get(options, (resp) => {
    let data = ''

    // A chunk of data has been recieved.
    resp.on('data', (chunk) => {
      data += chunk
    })

    // The whole response has been received. Print out the result.
    resp.on('end', () => {
      var A = JSON.parse(data)
      res.send(A)
    })
  }).on('error', (err) => {
    console.log('Error: ' + err.message)
  })
}
