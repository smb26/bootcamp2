var http = require('http')
var request = require('request')

module.exports = function (req, res, next) {
  var options = {
    host: '127.0.0.1',
    port: '5984',
    path: '/recipes/' + req.params.id,
    method: 'GET',
    headers: {
      'secret': 'bootcamp'
    }
  }

  http.get(options, (resp) => {
    let data = ''

    // A chunk of data has been recieved.
    resp.on('data', (chunk) => {
      data += chunk
    })

    // The whole response has been received. Print out the result.
    resp.on('end', () => {
      var A = JSON.parse(data)
      var id = A['_id']
      var rev = A['_rev']
      res.send(id + rev)
      request.put(
        'http://127.0.0.1:5984/recipes/' + id,
        { json: { 'name': req.body.name, 'age': req.body.age, '_rev': rev } },
        function (error, response, body) {
          if (!error && response.statusCode == 200) {
            console.log(body)
          }
          res.send('done')
        }
      )
    })
  }).on('error', (err) => {
    console.log('Error: ' + err.message)
  })
}
