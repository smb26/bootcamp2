var restify = require('restify')
var getdb = require('./getdb')
var getallids = require('./getallids')
var createdbuser = require('./createdbuser')
var deletedbuser = require('./deleteuser')
var updateuser = require('./updateuser')
var getbyname = require('./getbyname')

var server = restify.createServer()
server.use(restify.plugins.queryParser({ mapParams: true }))
server.use(restify.plugins.bodyParser({ mapParams: true }))
server.get('/db/user/:id', getdb)
server.get('/db/all', getallids)
server.post('/db/create/user', createdbuser)
server.del('/db/delete/user/:id', deletedbuser)
server.put('/db/update/user/:id', updateuser)
server.get('/db/user/byname/:name', getbyname)

server.listen(8080, function () {

})
