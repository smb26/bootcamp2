const http = require('http')
var request = require('request')

var mode = process.argv[2]

var getall = function () {
  var options = {
    host: '127.0.0.1',
    port: '5984',
    path: '/recipes/_design/namedoc/_view/allids',
    method: 'GET',
    headers: {
      'secret': 'bootcamp'
    }
  }

  http.get(options, (resp) => {
    let data = ''

    // A chunk of data has been recieved.
    resp.on('data', (chunk) => {
      data += chunk
    })

    // The whole response has been received. Print out the result.
    resp.on('end', () => {
      var A = JSON.parse(data)
      console.log(A)
      for (var i in A['rows'][0]) { console.log(i) }
      getdata(A['rows'], 0)
    })
  }).on('error', (err) => {
    console.log('Error: ' + err.message)
  })
}

getall()

var getdata = function (Array, i) {
  if (i >= Array.length) {} else {
    var options = {
      host: '127.0.0.1',
      port: '5984',
      path: '/recipes/' + Array[i]['id'],
      method: 'GET',
      headers: {
        'secret': 'bootcamp'
      }
    }

    http.get(options, (resp) => {
      let data = ''

      // A chunk of data has been recieved.
      resp.on('data', (chunk) => {
        data += chunk
      })

      // The whole response has been received. Print out the result.
      resp.on('end', () => {
        var A = JSON.parse(data)
        console.log(A['_id'] + A['name'])
        if (mode == 'age') {
          request.post(
            'http://127.0.0.1:5984/recipes',
            { json: { name: A['name'], age: 2018 - parseFloat(A['year']) } },
            function (error, response, body) {
              if (!error && response.statusCode == 200) {
                console.log(body)
              }
              funcdelete(Array, i)
            }
          )
        } else if (mode == 'year') {
          request.post(
            'http://127.0.0.1:5984/recipes',
            { json: { name: A['name'], year: 2018 - parseFloat(A['age'])} },
            function (error, response, body) {
              if (!error && response.statusCode == 200) {
                console.log(body)
              }
              funcdelete(Array, i)
            }
          )
        }
      })
    }).on('error', (err) => {
      console.log('Error: ' + err.message)
    })
  }
}

var funcdelete = function (Array, i) {
  if (i >= Array.length) {} else {
    var options = {
      host: '127.0.0.1',
      port: '5984',
      path: '/recipes/' + Array[i]['id'],
      method: 'GET',
      headers: {
        'secret': 'bootcamp'
      }
    }

    http.get(options, (resp) => {
      let data = ''

      // A chunk of data has been recieved.
      resp.on('data', (chunk) => {
        data += chunk
      })

      // The whole response has been received. Print out the result.
      resp.on('end', () => {
        var B = JSON.parse(data)
        var id = B['_id']
        var rev = B['_rev']
        console.log('to be deleted' + id + ' ' + rev)
        request.delete(
          'http://127.0.0.1:5984/recipes/' + id + '?rev=' + rev,
          function (error, response, body) {
            if (!error && response.statusCode == 200) {
              console.log(body)
            }
            console.log('done')
            getdata(Array, i + 1)
          }
        )
      })
    }).on('error', (err) => {
      console.log('Error: ' + err.message)
    })
  }
}
