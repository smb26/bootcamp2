var request = require('request')

module.exports = function (req, res, next) {
  request.post(
    'http://127.0.0.1:5984/recipes',
    { json: { name: req.body.name, age: req.body.age } },
    function (error, response, body) {
      if (!error && response.statusCode == 200) {
        console.log(body)
      }
      res.send('done')
    }
  )
}
