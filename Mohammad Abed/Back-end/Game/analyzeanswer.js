module.exports.analyzefunc =
{
  analyzeanswer: function (guess, answer) {
    var numguessed = guess.toString().substring(1, guess.length) // extract the number he entered using substring
    if (guess[0] == '>') { // check operator
      if (answer > numguessed) { // if his guess less than the answer
        return 'YES'
      } else {
        return 'NO'
      }
    } else if (guess[0] == '<') {
      if (numguessed > answer) { // if his guess is greater than the answer
        return 'YES'
      } else {
        return 'NO'
      }
    } else if (guess[0] == '=') {
      if (numguessed == answer) { // if his guess is equal the answer
        return 'Correct'
      } else {
        return 'NO'
      }
    }
  }
}
