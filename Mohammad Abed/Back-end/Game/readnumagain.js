
var analyzefunction = require('./analyzeanswer')

module.exports.readnumcont =
{
  readnumagain: function (randomnum) {
    const read = require('readline-sync')
    var name = read.question('Please Enter Your Name: ')
    const readline = require('readline')
    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout
    })
    var trials = 0

    var recursiveAsyncReadLine = function () {
      rl.question('Please guess a number: ', function (answer) {
        if (analyzefunction.analyzefunc.analyzeanswer(answer, randomnum) == 'Correct') { // we need some base case, for recursion
          console.log('Congratualtions, you guessed the number in ' + (trials + 1) + ' trials')
          var fs = require('fs')
          var hs = name + ' ' + (trials + 1) + '\n' // string containing name of participitant ans his score
          fs.appendFile('./test', hs, function (err) { // append any new highscore to file
            if (err) {
              return console.log(err)
            }
            trials = 0
          })

          return [rl.close(), trials + 1] // closing RL and returning from function.
        }
        console.log(analyzefunction.analyzefunc.analyzeanswer(answer, randomnum))
        recursiveAsyncReadLine() // Calling this function again to ask new question
        trials = trials + 1
      })
    }

    recursiveAsyncReadLine()
  }
}
