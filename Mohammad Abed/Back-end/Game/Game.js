var difficulty = require('./setdifficulty') // import dificulty function
var generaterandomnum = require('./generaterandomnum') // import generate number function
var readagain = require('./readnumagain') // import read again function
var readline = require('readline-sync') // import readline sync
var answer = readline.question('Please choose one from the following: \n 1. Start 2. View High Score 3. Quit \n') // declare a variable to save the answer

if (answer.toString() == '1') { // check the user input if it s 1 then run the game
  var randomnum = generaterandomnum.randomfunc.randomnum(1, difficulty.setdifficulty.difficulty()) // call the random number function with lower boundary = 1 and higher = randomnum based on difficulty level selected
  readagain.readnumcont.readnumagain(randomnum) // keep reading answers from the user
} else if (answer.toString() == '2') { // if user input is 2 then read the file and display the data
  var fs = require('fs') // require file
  fs.readFile('./test', {encoding: 'utf-8'}, function (err, data) { // read from the file
    if (err) throw err // throw an error is something happened
    console.log('High Scores : ')
    var cells = data.split('\n').map(function (el) { return el.split(/\s+/) }) // create an array of arrays containing name and score
    for (var itr in cells) {
      var temp = cells[itr][0]
      cells[itr][0] =parseFloat((cells[itr][1]))
      cells[itr][1] = temp
    } // swap array contents to sort according to score
    function sortNumber(a,b) {
    if(a[0]>b[0])
      return 1
    if(a[0]<b[0])
      return -1
    return 0
} // supporter function since javascript sort method sort alphabetically

    cells.sort(sortNumber)
    for (var i = 0; i < cells.length-1; i++) { console.log(cells[i][1] + ' ' + cells[i][0]) }
  })
} else if (answer.toString() == '3') { return }
