var express = require('express');
var app = express();

app.get('/:name', function(req, res){
  res.send(req.params.name);
});

app.listen(8888);