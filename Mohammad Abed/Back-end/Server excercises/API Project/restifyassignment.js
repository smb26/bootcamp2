var restify = require('restify')
var jsondata = require('./data.json')
var getfunction = require('./getdata')
var postfunction = require('./createdata')
var deletefunction = require('./deletedata')
var updatefunction = require('./updatedata')
var getallfunction = require('./getall')
var getdb = require('./getdb')

var server = restify.createServer()
server.use(restify.plugins.queryParser({ mapParams: true }))
server.use(restify.plugins.bodyParser({ mapParams: true }))
server.get('/user/:id', getfunction)
server.get('/db/user/:id', getdb)
server.post('/user', postfunction)
server.del('/user', deletefunction)
server.put('/user', updatefunction)
server.get('/users', getallfunction)

server.listen(8080, function () {

})
