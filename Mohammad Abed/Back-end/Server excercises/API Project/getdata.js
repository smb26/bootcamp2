var restify = require('restify')
var jsondata = require('./data.json')
var errors = require('restify-errors')
module.exports = function (req, res, next) {
  if (req.header('secret') == jsondata['secret']) {
    var exists = false
    var index = null
    for (var i = 0; i < jsondata['people'].length; i++) {
      if (parseFloat(jsondata['people'][i]['id']) == req.params.id) {
        exists = true
        index = i
        break
      }
    }
    if (exists == true) {
      res.send(jsondata['people'][index])
      console.log('done')
    } else if (exists == false) {
      return next(new errors.NotFoundError('User Not Found and unable to get!'))
    }
    next()
   } else {
    return next(new errors.UnauthorizedError('Invalid Key'))
  }
}
