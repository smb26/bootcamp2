var request = require("request");
var userDetails;

function getdata() {
    // Setting URL and headers for request
    var options = {
      // will be ignored
      method: 'GET',
      uri: 'http://localhost:8080/user/?id=2',

      // HTTP Archive Request Object
      har: {
        url: 'http://localhost:8080/user/?id=2',
        method: 'get',
        headers: [

          {
            name: 'secret',
            value: 'bootcamp'
          }
        ],
        postData: {
          mimeType: 'application/x-www-form-urlencoded',
        }
      }
    }
    // Return new promise 
    return new Promise(function(resolve, reject) {
        // Do async job
        request.get(options, function(err, resp, body) {
            if (err) {
                reject(err);
            } else {
                resolve(JSON.parse(body));
            }
        })
    })

}


function main() {
    var initializePromise = initialize();
    initializePromise.then(function(result) {
        userDetails = result;
        console.log(userDetails)
    }, function(err) {
        console.log(err);
    })
}

main();