var restify = require('restify')
var jsondata = require('./data.json')
var errors = require('restify-errors')
var fs = require('fs')

module.exports = function (req, res, next) {
  if (req.header('secret') == jsondata['secret']) {
    var exists = false
    for (var i = 0; i < jsondata['people'].length; i++) {
      if (parseFloat(jsondata['people'][i]['id']) == req.body.id) {
        exists = true
        break
      }
    }
    if (exists == false) {
      var newperson = {'id': req.body.id.toString(), 'name': req.body.name.toString(), 'age': req.body.age.toString()}
      jsondata['people'].push(newperson)

      fs.writeFile('./data.json', JSON.stringify(jsondata), function (err) {
        if (err) {
          return console.log(err)
        }
      })
      res.send('user added')

      return next()
    } else {
      return next(new errors.BadRequestError('User already exists !'))
    }
  } else {
    return next(new errors.UnauthorizedError('Invalid Key'))
  }
}
