const http = require('http')
var request = require('request')
var jsondata = require('./data.json')
var jsondataup = require('./updateddata.json')
var errors = require('restify-errors')
var fs = require('fs')

var getall = function () {
  var options = {
    host: 'localhost',
    port: '8080',
    path: '/users',
    method: 'GET',
    headers: {
      'secret': 'bootcamp'
    }
  }

  http.get(options, (resp) => {
    let data = ''

    // A chunk of data has been recieved.
    resp.on('data', (chunk) => {
      data += chunk
    })

    // The whole response has been received. Print out the result.
    resp.on('end', () => {
      var A = JSON.parse(data)
      getdata(A, 0)
    })
  }).on('error', (err) => {
    console.log('Error: ' + err.message)
  })
}

var getdata = function (Array, i) {
  if (i >= Array.length) {} else {
    var options = {
      host: 'localhost',
      port: '8080',
      path: '/user/?id=' + Array[i],
      method: 'GET',
      headers: {
        'secret': 'bootcamp'
      }
    }

    http.get(options, (resp) => {
      let data = ''

      // A chunk of data has been recieved.
      resp.on('data', (chunk) => {
        data += chunk
      })

      // The whole response has been received. Print out the result.
      resp.on('end', () => {
        var A = JSON.parse(data)
        console.log(A)
        var obj = {'id': A['id'], 'name': A['name'], 'year': A['age']}
        jsondataup['people'].push(obj)
        fs.writeFile('./updateddata.json', JSON.stringify(jsondataup), function(err){ deleteuser(Array, i)})
      })
    }).on('error', (err) => {
      console.log('Error: ' + err.message)
    })
  }
}

var deleteuser = function (Array, i) {
  if (i >= Array.length) {} else {
    request({
      // will be ignored
      method: 'DELETE',
      uri: 'http://localhost:8080/user',

      // HTTP Archive Request Object
      har: {
        url: 'http://localhost:8080/user',
        method: 'DELETE',
        headers: [

          {
            name: 'secret',
            value: 'bootcamp'
          }
        ],
        postData: {
          mimeType: 'application/x-www-form-urlencoded',
          params: [
            {
              name: 'id',
              value: Array[i]
            }
          ]
        }
      }
    }, getdata(Array, i + 1))
  }
}

getall()
