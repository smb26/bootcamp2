var restify = require('restify')
var jsondata = require('./data.json')
var errors = require('restify-errors')
var fs = require('fs')

module.exports = function (req, res, next) {
  if (req.header('secret') == jsondata['secret']) {
    var exists = false
    var index = null
    for (var i = 0; i < jsondata['people'].length; i++) {
      if (parseFloat(jsondata['people'][i]['id']) == req.body.id) {
        exists = true
        index = i
        break
      }
    }
    if (exists == true) {
      jsondata['people'].splice(index, 1)

      fs.writeFile('./data.json', JSON.stringify(jsondata), function (err) {
        if (err) {
          return console.log(err)
        }
      })
      res.send('User Deleted Successfully')
    } else if (exists == false) {
      return next(new errors.BadRequestError('User does not exist !'))
    }
  } else {
    return next(new errors.UnauthorizedError('Invalid Key'))
  }
}
