var jsondata = require('./data.json')
var errors = require('restify-errors')
module.exports = function (req, res, next) {
  if (req.header('secret') == jsondata['secret']) {
    var resultarray = []
    for (var i = 0; i < jsondata['people'].length; i++) {
      resultarray.push(jsondata['people'][i]['id'])
    }

    res.send(resultarray)
  } else {
    return next(new errors.UnauthorizedError('Invalid Key'))
  }
}
