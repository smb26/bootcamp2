import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import thelist from '@/components/thelist'
import matrix from '@/components/matrix'
Vue.use(Router)

export default new Router({
  routes: [{
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/list',
      name: 'List',
      component: thelist
    },
    {
      path: '/matrix',
      name: 'Matrix',
      component: matrix
    },
  ]
})
