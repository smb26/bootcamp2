import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);
export default new Vuex.Store({
  state: {
    message: "",
    itemslist: [],
    index: "",
    itemslist1: [],
    itemslist2: [],
    rows: "",
    columns: ""
  },
  getters: {
    message(state) {
      return state.message;
    },
    itemslist(state) {
      return state.itemslist;
    },
    index(state) {
      return state.index;
    },
    itemslist1(state) {
      return state.itemslist1;
    },
    itemslist2(state) {
      return state.itemslist2;
    }
  },
  mutations: {
    message(state, payload) {
      state.message = payload;
    },
    itemslist(state, payload) {
      state.itemslist = payload;
    },
    index(state, payload) {
      state.index = payload;
    },
    itemslist1(state, payload) {
      state.itemslist1 = payload;
    },
    itemslist2(state, payload) {
      state.itemslist2 = payload;
    },
    itemslist1push(state) {
      state.itemslist1.push(1);
    },
    itemslist2push(state) {
      state.itemslist2.push(1);
    },
    setrows(state, payload) {
      state.rows = payload;
    },
    setcolumns(state, payload) {
      state.columns = payload;
    }


  },

});
