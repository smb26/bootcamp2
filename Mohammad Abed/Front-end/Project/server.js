var restify = require ('restify');
var server = restify.createServer();
var postdata = require('./savebooking');
var latestdata = require('./latestbooking');
server.use(restify.plugins.bodyParser({ mapParams: true }))
server.post('/postdata',postdata);
server.get('/latestdata',latestdata);
server.listen(8888, function(){
    console.log("Server is up and running!");
});