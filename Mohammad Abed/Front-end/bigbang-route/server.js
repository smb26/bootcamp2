var restify = require('restify');
var server = restify.createServer();
var latestdata = require('./latest')
var putmonth = require('./updatemonth')
var create = require('./create')
server.use(restify.plugins.bodyParser({
  mapParams: true
}))
server.post('/saveday', putmonth);
server.get('/latest', latestdata)
server.post('/savemonth', putmonth)
server.post('/savetime', putmonth)
server.post('/saveseatnum', putmonth)
server.post('/savepayment', putmonth)
server.post('/savevoucher', putmonth)
server.post('/create', create)
server.listen(8888, function () {
  console.log("Server is up and running!");
});
