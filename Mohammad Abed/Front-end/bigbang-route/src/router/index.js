import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import day from '@/components/day'
import month from '@/components/month'
import sessiontime from '@/components/sessiontime'
import seatnum from '@/components/seatnum'
import payment from '@/components/payment'
import voucher from '@/components/voucher'
import success from '@/components/success'
import create from '@/components/create'

Vue.use(Router)

export default new Router({
  routes: [{
      path: '/',
      name: 'create',
      component: create
    },
    {
      path: '/day',
      name: 'day',
      component: day
    },
    {
      path: '/month',
      name: 'month',
      component: month
    },
    {
      path: '/time',
      name: 'sessiontime',
      component: sessiontime
    },
    {
      path: '/seatnum',
      name: 'seatnum',
      component: seatnum
    },
    {
      path: '/payment',
      name: 'payment',
      component: payment
    },
    {
      path: '/voucher',
      name: 'voucher',
      component: voucher
    },
    {
      path: '/success',
      name: 'success',
      component: success
    }
  ]
})
