const nano = require("nano")("http://localhost:5984");
const db = nano.db.use('bigbangroute');

module.exports = function (req, res, next) {
  db.get(req.body.id).then(body => {
    res.send(body);
  })
};
