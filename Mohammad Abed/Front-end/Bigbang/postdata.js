const nano = require('nano')('http://localhost:5984');
const db = nano.db.use('bigbang');
module.exports = function (req, res) {
    db.insert({
        "Day": req.body["day"],
        "Date": req.body["month"],
        "Time": req.body["time"],
        "Number of seats": req.body["seatnum"],
        "Payment Method": req.body["payment"],
        "Voucher Code": req.body["voucher"]
    }).then((body) => {
        res.send(body)
    })

}