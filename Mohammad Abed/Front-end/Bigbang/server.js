var restify = require('restify');
var server = restify.createServer();
var postdata = require('./postdata');
var latestdata = require('./latest')
server.use(restify.plugins.bodyParser({
    mapParams: true
}))
server.post('/save', postdata);
server.get('/latestdata', latestdata);
server.listen(8888, function () {
    console.log("Server is up and running!");
});