import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);
export default new Vuex.Store({
  state: {
    day: "",
    date: "",
    time: "",
    seatnum: "",
    payment: "",
    voucher: ""
  },
  getters: {
    day(state) {
      return state.day;
    },
    date(state) {
      return state.date;
    },
    sessiontime(state) {
      return state.time;
    },
    seatnum(state) {
      return state.seatnum;
    },
    payment(state) {
      return state.payment;
    },
    voucher(state) {
      return state.voucher;
    }
  },
  mutations: {
    day(state, payload) {
      state.day = payload;
    },
    date(state, payload) {
      state.date = payload;
    },
    voucher(state, payload) {
      state.voucher = payload;
    },
    sessiontime(state, payload) {
      state.time = payload;
    },
    seatnum(state, payload) {
      state.seatnum = payload;
    },
    payment(state, payload) {
      state.payment = payload;
    }

  }

});
