var restify = require('restify');
var server = restify.createServer();
var create = require('./create')
server.use(restify.plugins.bodyParser({
  mapParams: true
}))
server.post('/create', create)
server.listen(8888, function () {
  console.log("Server is up and running!");
});
