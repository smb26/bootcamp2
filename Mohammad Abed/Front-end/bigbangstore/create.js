const nano = require("nano")("http://localhost:5984");
const db = nano.db.use('bigbangroute');

module.exports = function (req, res, next) {
  db.insert({
    day: req.body["day"],
    month: req.body["date"],
    time: req.body["time"],
    seatnum: req.body["seatnum"],
    payment: req.body["payment"],
    voucher: req.body["voucher"]

  }).then(data => {
    res.send(data);
  })
};
