var canvas = document.querySelector('canvas');

canvas.width = window.innerWidth;
canvas.height = window.innerHeight/2;

var ctx = canvas.getContext('2d');

function Circle(x,y,dx,dy,radius){
	this.x=x;
	this.y=y;
	this.dx=dx;
	this.dy=dy;
	this.radius=radius;

	this.draw = function() {
		ctx.beginPath();
		ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2,false);
		ctx.stroke();
	}

	this.update = function() {
		 if(this.x+this.radius > innerWidth || this.x-this.radius<0) {
		 	this.dx = -this.dx;
		 }

		 if(this.y+this.radius >innerHeight/2 || this.y-this.radius<=0) {
		 	this.dy = -this.dy;
		 }

		 this.x += this.dx;
		 this.y += this.dy;

		 this.draw();


	}
}

var array = [];

for(var j=0;j<10;j++) {
	var radius = 30;
	var x = Math.floor(Math.random() * ((innerWidth-radius) - radius + 1)) + radius;
	var y = Math.floor(Math.random() * ((innerHeight/2-radius)- radius  + 1)) + radius;
	var dx = (Math.random() - 0.5) * 8;
	var dy = (Math.random() - 0.5) * 8;
	var circle = new Circle (x,y,dx,dy,radius);
	array.push(circle);
}
function animate () {
	requestAnimationFrame(animate);
	ctx.clearRect(0,0,innerWidth,innerHeight);
	for(var k=0; k<array.length;k++)
	array[k].update();
}
animate();
