import Vue from "vue";
import App from "./App.vue";
import VueRouter from "vue-router";
import matrix from "@/components/matrix";
import thelist from "@/components/thelist";

//Vue.config.productionTip = false;
Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "list",
    component: thelist
  },
  {
    path: "/matrix",
    name: "matrix",
    component: matrix
  }
];
const router = new VueRouter({
  routes
});

new Vue({
  render: h => h(App),
  router
}).$mount("#app");
