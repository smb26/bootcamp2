import Vue from "vue";
import Router from "vue-router";
import matrix from "@/components/matrix.vue";
import thelist from "@/components/thelist.vue";
Vue.use(Router);
export default new Router({
  routes: [
    {
      path: "/",
      name: "matrix",
      component: matrix
    },
    {
      path: "/",
      name: "list",
      component: thelist
    }
  ]
});
