Make a new directory called pages.
Created 3 html files called page_1, page_2 and page_3 inside the pages directory.
In our main html page (3.html) add 3 links, each taking the user to the pages we created in the previous step.
Finally, the user should always have a link in the 3 pages to go back to our main html page.