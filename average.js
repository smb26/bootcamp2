const data1 = require("./grades");
var data = data1.data;

exports.getAvg = function(name, course) {
	// this will hold the average of the student
	var grades = 0;
	var numberOfGrades = 0;

	//since this function has optional parameter, then must check if the
	// optional parameter is undefined
	// you can also write if(course) without the need to provide undefined
	if(course === undefined) {
		// now in here we are getting the every key and looping over them
		for(var key in data) {
			// if the key matches the name of the argument provided
			// then we will look into his courses
			if(key == name) {
				// accessing the course of the provided key (name of student)
				var courses = data[key];
				for(var key2 in courses) {
					
					var gradesArray = courses[key2];
					//console.log(gradesArray);
					//now we are trying to get each grade and add it to the variable grades
					for(var i=0; i<gradesArray.length; i++) {
						grades += gradesArray[i];
						// since we checked a grade for a course then we should increment numberOfGrades
						// by one since we are going to divide grades by numberOfGrades
						numberOfGrades++;
						// this was used for testing and debugging purposes
						//console.log(grades);
					}
				}	
			}
		}
		return console.log( grades/numberOfGrades );
	}// the ending of the most outer IF-statement
	else{
		for(var key in data){
			// checking if the name matches the key, if yes we fetch it
			if(key == name) {
				// accessing the course of the provided key (name of student)
				var courses = data[key];
				for(var key2 in courses) {
					// if the key2 mathces the name of the course then we excute what
					// is inside the if-statement
					if(key2 == course){
						var gradesArray = courses[key2];
						for(var i=0; i<gradesArray.length; i++) {
							grades += gradesArray[i];
							// since we checked a grade for a course then we should increment numberOfGrades
							// by one since we are going to divide grades by numberOfGrades
							numberOfGrades++;
							// this was used for testing and debugging purposes
							//console.log(grades);
						}
					}
				}
			}
		}
		return console.log( grades/numberOfGrades );
	}// the end of the else statement

}