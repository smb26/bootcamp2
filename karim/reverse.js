
const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question('Please enter a word: ', (str) => {


    function reverseString(str) {

        var splitString = str.split("");


        var reverseArray = splitString.reverse();

        var joinArray = reverseArray.join("");

        return joinArray;
    }


    console.log(reverseString(str));
    rl.close();

})