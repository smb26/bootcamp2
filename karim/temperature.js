const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question('Please enter a temperature : ', (a) => {
    var t= new String(a);
    var last_character = t[t.length-1];
    if(last_character=='C')
    {
        tempC = t.substring(0, t.length - 1);
        tempF = (tempC * (9 / 5)) + 32;
        tempK = tempC + 273.15;
    }
    else if(last_character=='F')
    {
        tempF = t.substring(0, t.length - 1);
        tempC = (tempF - 32) * (5 / 9);
        tempK = (tempF + 459.67) * (5 / 9);
    }
    else
    {
        tempK = t.substring(0, t.length - 1);
        tempF = (tempK * (9 / 5)) - 459.67;
        tempC = tempK - 273.15;

    }
    console.log(tempC ,tempF, tempK);
    rl.close();
});
