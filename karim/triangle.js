const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question('Please enter the first side of triangle value : ', (side1) => {


    rl.question('Please enter the second number : ', (side2) => {


        rl.question('Please enter the third side : ', (side3) => {

            this.side1 = side1;
            this.side2 = side2;
            this.side3 = side3;

            this.getArea = function(){
                let p = this.getPerimeter();
                return Math.sqrt(p * (p-parseFloat(this.side1)) * (p-parseFloat(this.side2)) * (p-parseFloat(this.side3)));
            };

            this.getPerimeter = function(){
                return parseFloat(this.side1) + parseFloat(this.side2) + parseFloat(this.side3);
            };

            this.toString = function(){
                return "The Triangle with sides = " + parseFloat(this.side1) + ", " +  parseFloat(this.side2)+ ", " +  parseFloat(this.side3) + " has Area = " + this.getArea() + " and Perimeter = " + this.getPerimeter();
            };



            var s1 = parseFloat(side1.value)
                , s2 = parseFloat(side2.value)
                , s3 = parseFloat(side3.value);


            console.log(`The sum of above two numbers is ${this.toString()}`);

        rl.close()
    })})
});