var arr = [2, 11, 37, 42, 34, 25, 78, 22];

function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;


    while (0 !== currentIndex) {


        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;


        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}



arr = shuffle(arr);
console.log(arr);