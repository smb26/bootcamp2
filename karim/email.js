const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question('Please enter an email: ', (a) => {

    var emailFilter = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/;

    function validate() {
        if (!emailFilter.test(a)) {

            return false;
        }
        return "true"
    }
console.log(validate());
rl.close();

})