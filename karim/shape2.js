function p(h, w) {
    w = w || 1;
    if (h === 1) {
        console.log('x'.repeat(w));
    } else if (h > 1) {
        console.log(' '.repeat(h-1) + 'x'.repeat(w));
        p(h-1, w+2);
    }
}

p(5);